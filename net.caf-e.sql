/*
Navicat MySQL Data Transfer

Source Server         : ads
Source Server Version : 50722
Source Host           : 159.89.42.146:3306
Source Database       : net.caf-e

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-06-29 12:11:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agendamentos
-- ----------------------------
DROP TABLE IF EXISTS `agendamentos`;
CREATE TABLE `agendamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_pessoa` int(11) NOT NULL,
  `data_inicial` datetime NOT NULL,
  `data_final` datetime NOT NULL,
  `cod_tipo_agendamento` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agendamentos
-- ----------------------------

-- ----------------------------
-- Table structure for atividade
-- ----------------------------
DROP TABLE IF EXISTS `atividade`;
CREATE TABLE `atividade` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária da tabela',
  `cod_pessoa` int(11) NOT NULL COMMENT 'chave estrangeira referenciando a pessoa',
  `data` datetime DEFAULT NULL COMMENT 'data da atividade realizada, digitada pelo usuário',
  `data_ts` timestamp NULL DEFAULT NULL COMMENT 'Data gerada pelo sistema',
  `restricao` int(1) DEFAULT '0' COMMENT 'define se existe restrição',
  `cod_atividade` int(11) DEFAULT NULL COMMENT 'chave estrangeira referenciando o nome da atividade',
  `descricao` text COMMENT 'Descrição da atividade Realizada',
  `cod_usuario` int(11) DEFAULT NULL COMMENT 'chave estrangeira referenciando o profissional que realizou a atividade',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of atividade
-- ----------------------------
INSERT INTO `atividade` VALUES ('1', '2', '2018-06-28 13:39:24', '2018-06-29 13:39:24', '0', '3', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', '1');
INSERT INTO `atividade` VALUES ('2', '4', '2018-06-21 13:39:48', '2018-06-29 13:39:48', '0', '3', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', '1');
INSERT INTO `atividade` VALUES ('3', '3', '2018-06-29 13:40:11', '2018-06-29 13:40:11', '0', '3', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', '1');
INSERT INTO `atividade` VALUES ('4', '3', '2018-06-29 13:45:45', '2018-06-29 13:45:45', '0', '3', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', '1');
INSERT INTO `atividade` VALUES ('5', '3', '2018-06-01 13:46:50', '2018-06-29 13:46:50', '0', '2', 'Lorem ipsum augue sagittis aliquet facilisis et nullam justo euismod netus dolor accumsan eget quam aliquet ipsum, accumsan cursus nam magna placerat urna mattis ipsum nibh justo fusce quam inceptos commodo vel. duis sagittis integer quis at placerat luctus, venenatis placerat turpis potenti vivamus tristique, quam fringilla cras venenatis etiam. ', '1');
INSERT INTO `atividade` VALUES ('6', '3', '2018-06-08 13:47:11', '2018-06-29 13:47:11', '0', '2', 'Lorem ipsum augue sagittis aliquet facilisis et nullam justo euismod netus dolor accumsan eget quam aliquet ipsum, accumsan cursus nam magna placerat urna mattis ipsum nibh justo fusce quam inceptos commodo vel. duis sagittis integer quis at placerat luctus, venenatis placerat turpis potenti vivamus tristique, quam fringilla cras venenatis etiam. ', '1');
INSERT INTO `atividade` VALUES ('7', '2', '2018-06-22 13:47:49', '2018-06-29 13:47:49', '0', '2', 'Lorem ipsum augue sagittis aliquet facilisis et nullam justo euismod netus dolor accumsan eget quam aliquet ipsum, accumsan cursus nam magna placerat urna mattis ipsum nibh justo fusce quam inceptos commodo vel. duis sagittis integer quis at placerat luctus, venenatis placerat turpis potenti vivamus tristique, quam fringilla cras venenatis etiam. ', '1');

-- ----------------------------
-- Table structure for atividade_lista
-- ----------------------------
DROP TABLE IF EXISTS `atividade_lista`;
CREATE TABLE `atividade_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da tabela',
  `atividade` varchar(45) NOT NULL COMMENT 'Nome da atividade',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of atividade_lista
-- ----------------------------
INSERT INTO `atividade_lista` VALUES ('1', 'Visita domiciliar');
INSERT INTO `atividade_lista` VALUES ('2', 'Atendimento piscologico');
INSERT INTO `atividade_lista` VALUES ('3', 'Atendimento');
INSERT INTO `atividade_lista` VALUES ('4', 'Encaminhamento');

-- ----------------------------
-- Table structure for bairro
-- ----------------------------
DROP TABLE IF EXISTS `bairro`;
CREATE TABLE `bairro` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `bairro` varchar(45) NOT NULL COMMENT 'Nome do bairro',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bairro
-- ----------------------------
INSERT INTO `bairro` VALUES ('1', 'ALFA SUL');
INSERT INTO `bairro` VALUES ('2', 'BAIXADA');
INSERT INTO `bairro` VALUES ('3', 'BARREIRO DE BAIXO');
INSERT INTO `bairro` VALUES ('4', 'BARREIRO DE CIMA');
INSERT INTO `bairro` VALUES ('5', 'BELA VISTA');
INSERT INTO `bairro` VALUES ('6', 'BOM JARDIM');
INSERT INTO `bairro` VALUES ('7', 'BOM JESUS DE REALEZA');
INSERT INTO `bairro` VALUES ('8', 'BOM PASTOR');
INSERT INTO `bairro` VALUES ('9', 'CATUAI');
INSERT INTO `bairro` VALUES ('10', 'CENTRO');
INSERT INTO `bairro` VALUES ('11', 'COLINA');
INSERT INTO `bairro` VALUES ('12', 'COQUEIRO');
INSERT INTO `bairro` VALUES ('13', 'COQUEIRO RURAL');
INSERT INTO `bairro` VALUES ('14', 'DOM CORREA');
INSERT INTO `bairro` VALUES ('15', 'ENGENHO DA SERRA');
INSERT INTO `bairro` VALUES ('16', 'LAJINHA');
INSERT INTO `bairro` VALUES ('17', 'LUCIANO HERINGER');
INSERT INTO `bairro` VALUES ('18', 'MANHUAÇULZINHO');
INSERT INTO `bairro` VALUES ('19', 'MATINHA');
INSERT INTO `bairro` VALUES ('20', 'MONTE ALVERNE');
INSERT INTO `bairro` VALUES ('21', 'NOSSA SENHORA APARECIDA');
INSERT INTO `bairro` VALUES ('22', 'OPERARIOS');
INSERT INTO `bairro` VALUES ('23', 'PALMEIRAS');
INSERT INTO `bairro` VALUES ('24', 'PETRINA');
INSERT INTO `bairro` VALUES ('25', 'PINHEIRO');
INSERT INTO `bairro` VALUES ('26', 'PINHEIRO II');
INSERT INTO `bairro` VALUES ('27', 'PONTE DA ALDEIA');
INSERT INTO `bairro` VALUES ('28', 'PONTE DO SILVA');
INSERT INTO `bairro` VALUES ('29', 'POUSO ALEGRE');
INSERT INTO `bairro` VALUES ('30', 'REALEZA');
INSERT INTO `bairro` VALUES ('31', 'SACRAMENTO');
INSERT INTO `bairro` VALUES ('32', 'SAGRADA FAMILIA');
INSERT INTO `bairro` VALUES ('33', 'SANTA LUZIA');
INSERT INTO `bairro` VALUES ('34', 'SANTA TEREZINHA');
INSERT INTO `bairro` VALUES ('35', 'SANTANA');
INSERT INTO `bairro` VALUES ('36', 'SANTO AMARO');
INSERT INTO `bairro` VALUES ('37', 'SANTO ANTONIO');
INSERT INTO `bairro` VALUES ('38', 'SAO FRANCISCO DE ASSIS');
INSERT INTO `bairro` VALUES ('39', 'SAO JORGE');
INSERT INTO `bairro` VALUES ('40', 'SAO PEDRO DO AVAI');
INSERT INTO `bairro` VALUES ('41', 'SAO VICENTE');
INSERT INTO `bairro` VALUES ('42', 'SOLEDADE');
INSERT INTO `bairro` VALUES ('43', 'TODOS OS SANTOS');
INSERT INTO `bairro` VALUES ('44', 'VILA CACHOEIRINHA');
INSERT INTO `bairro` VALUES ('45', 'VILA DE FATIMA');
INSERT INTO `bairro` VALUES ('46', 'VILA DEOLINDA');
INSERT INTO `bairro` VALUES ('47', 'VILA FORMOSA');
INSERT INTO `bairro` VALUES ('48', 'VILA NOVA');
INSERT INTO `bairro` VALUES ('49', 'PONTE DO EVARISTO');
INSERT INTO `bairro` VALUES ('50', 'VILA BOA ESPERANÃ‡A');
INSERT INTO `bairro` VALUES ('51', 'ROÇA GRANDE');

-- ----------------------------
-- Table structure for beneficio
-- ----------------------------
DROP TABLE IF EXISTS `beneficio`;
CREATE TABLE `beneficio` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária da tabela',
  `cod_pessoa` int(11) NOT NULL COMMENT 'Chave estrangeira referenciando a tabela pessoas',
  `data` datetime DEFAULT NULL COMMENT 'Data em que o benefício foi concedido',
  `data_ts` timestamp NULL DEFAULT NULL COMMENT 'Data da criação do registro',
  `cod_beneficio` int(11) DEFAULT NULL COMMENT 'Chave estrangeira referenciando a tabela benefícios_lista',
  `quantidade` varchar(45) DEFAULT NULL COMMENT 'Quantidade do que foi beneficiado',
  `descricao` text COMMENT 'Comentários sobre o benefício',
  `cod_usuario` int(11) DEFAULT NULL COMMENT 'Chave estrangeira referenciando a tabela usuarios',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of beneficio
-- ----------------------------
INSERT INTO `beneficio` VALUES ('1', '4', '2018-06-22 13:48:36', '2018-06-29 13:48:36', '1', '1', 'Lorem ipsum lobortis elementum, mollis fermentum taciti consectetur, maecenas duis', '1');
INSERT INTO `beneficio` VALUES ('2', '4', '2018-06-28 13:48:49', '2018-06-29 13:48:49', '3', '2', 'Lorem ipsum lobortis elementum, mollis fermentum taciti consectetur, maecenas duis', '1');
INSERT INTO `beneficio` VALUES ('3', '4', '2018-06-29 13:49:08', '2018-06-29 13:49:08', '2', '200', 'pagamento de conta de luz', '1');
INSERT INTO `beneficio` VALUES ('4', '3', '2018-06-21 13:54:38', '2018-06-29 13:54:38', '1', '2', '	Ultricies ut orci a neque at, phasellus sed class volutpat. ', '1');
INSERT INTO `beneficio` VALUES ('5', '3', '2018-06-28 13:54:57', '2018-06-29 13:54:57', '3', '3', '	Cubilia non purus vel suspendisse dolor, maecenas pretium venenatis congue. ', '1');
INSERT INTO `beneficio` VALUES ('6', '1', '2018-06-29 13:55:44', '2018-06-29 13:55:44', '2', '150', 'pagamento da conta de agua atrasada', '1');

-- ----------------------------
-- Table structure for beneficio_lista
-- ----------------------------
DROP TABLE IF EXISTS `beneficio_lista`;
CREATE TABLE `beneficio_lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da tabela',
  `beneficio` varchar(45) NOT NULL COMMENT 'Nome do benefício',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of beneficio_lista
-- ----------------------------
INSERT INTO `beneficio_lista` VALUES ('1', 'Cesta basica');
INSERT INTO `beneficio_lista` VALUES ('2', 'Pagamento de conta');
INSERT INTO `beneficio_lista` VALUES ('3', 'Doacao de roupa');

-- ----------------------------
-- Table structure for cidades
-- ----------------------------
DROP TABLE IF EXISTS `cidades`;
CREATE TABLE `cidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária da tabela',
  `cidade` varchar(45) NOT NULL COMMENT 'Nome da tabela',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cidades
-- ----------------------------

-- ----------------------------
-- Table structure for contato
-- ----------------------------
DROP TABLE IF EXISTS `contato`;
CREATE TABLE `contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da tabela',
  `data_cadastro` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do registro',
  `nome` varchar(45) NOT NULL COMMENT 'Nome do contato',
  `nascimento` date NOT NULL COMMENT 'Data de nascimento',
  `endereco` varchar(45) DEFAULT NULL COMMENT 'Endereço do contato',
  `numero` int(5) DEFAULT NULL COMMENT 'Número da residência',
  `bairro` varchar(60) DEFAULT NULL COMMENT 'Chave estrangeira refenciando a tabela bairros',
  `cidade` varchar(40) DEFAULT NULL COMMENT 'Chave estrangeira referenciando a tabela cidades',
  `referencia` varchar(100) DEFAULT NULL COMMENT 'Complemento ou referencia da residencia',
  `telefone` varchar(50) DEFAULT NULL COMMENT 'Telefone para contato',
  `email` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contato
-- ----------------------------
INSERT INTO `contato` VALUES ('1', '2018-06-04 10:45:58', 'Luiz Soares', '1993-04-30', 'Rua A', '0', 'Centro', 'Martins Soares', 'Fim Do Mundo', '(33)98401-7757', 'flavioworks@live.com');
INSERT INTO `contato` VALUES ('2', '2018-06-04 10:49:23', 'Joao Victor', '1999-10-10', 'Ceu Azul ', '0', 'Matinha', 'Manhuaçu', '', '(33)03331-0909', 'vitas@live.com');
INSERT INTO `contato` VALUES ('3', '2018-06-12 00:16:53', 'Marlon Jose', '1989-03-01', 'Ru A', '999', 'Centro', 'Curitiba', '', '(33)03332-3800', 'marlonsa@gmail.com');
INSERT INTO `contato` VALUES ('4', '2018-06-19 11:41:39', 'Roberto Carlos', '1985-12-10', 'Rua Das Ameixas Pretas', '190', 'Uzina', 'Martins Soares', '', '(33)98401-7758', 'roro@gmai.com');

-- ----------------------------
-- Table structure for oficina
-- ----------------------------
DROP TABLE IF EXISTS `oficina`;
CREATE TABLE `oficina` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária na tabela',
  `data_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data da oficina',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'Status se a oficina está ativa ou não',
  `oficina` varchar(45) NOT NULL COMMENT 'nome da oficina',
  `oficineiro` varchar(45) NOT NULL,
  `cod_usuario` int(11) NOT NULL COMMENT 'Chave estrangeira referenciando a tabela usuarios',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oficina
-- ----------------------------
INSERT INTO `oficina` VALUES ('1', '2018-06-29 13:57:29', '1', 'Boxe', 'Leandro Machado', '1');
INSERT INTO `oficina` VALUES ('2', '2018-06-29 13:58:01', '1', 'Grafite', 'Fabio (cras)', '1');

-- ----------------------------
-- Table structure for oficina_inscrito
-- ----------------------------
DROP TABLE IF EXISTS `oficina_inscrito`;
CREATE TABLE `oficina_inscrito` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária da tabela',
  `cod_oficina` int(11) NOT NULL COMMENT 'Chave estrangeira referenciando a tabela de oficinas',
  `cod_pessoa` int(11) DEFAULT NULL COMMENT 'Chave estrangeira refenciando a tabela pessoa',
  `cod_usuario` int(11) DEFAULT NULL COMMENT 'Chave estrangeira referenciando o profissional na tabela profissional',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oficina_inscrito
-- ----------------------------
INSERT INTO `oficina_inscrito` VALUES ('1', '2', '1', '1');
INSERT INTO `oficina_inscrito` VALUES ('2', '1', '4', '1');
INSERT INTO `oficina_inscrito` VALUES ('3', '1', '1', '1');

-- ----------------------------
-- Table structure for pessoa
-- ----------------------------
DROP TABLE IF EXISTS `pessoa`;
CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave Primária',
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de criação do cadastro',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT 'define se o registro está ativo ou não',
  `nome` varchar(45) NOT NULL COMMENT 'Nome completo da pessoa',
  `nome_social` varchar(45) NOT NULL COMMENT 'Apelido ou abreviação do nome da pessoa',
  `nascimento` date NOT NULL COMMENT 'data de nascimento da pessoa',
  `cod_sexo` int(1) NOT NULL DEFAULT '0' COMMENT 'chave estrangeira da tabela sexo',
  `cod_familiar` varchar(15) DEFAULT '0' COMMENT 'codigo se esta associado a uma família',
  `cpf` varchar(14) DEFAULT NULL COMMENT 'cpf da pessoa',
  `rg` varchar(14) DEFAULT NULL COMMENT 'RG da pessoa',
  `rg_uf` varchar(2) DEFAULT NULL COMMENT 'estado em que foi emitido o RG',
  `cn` varchar(45) DEFAULT NULL COMMENT 'Certidão de Nascimento',
  `ctps` varchar(45) DEFAULT NULL COMMENT 'Carteira de Trabalho',
  `pai` varchar(45) DEFAULT NULL COMMENT 'Nome do pai',
  `mae` varchar(45) DEFAULT NULL COMMENT 'nome da Mae',
  `endereco` varchar(45) DEFAULT NULL COMMENT 'Endereço em que mora (Rua)',
  `numero` int(5) DEFAULT NULL COMMENT 'Número da Casa',
  `cod_bairro` int(11) DEFAULT NULL COMMENT 'chave estrangeira referenciando o bairro',
  `referencia` varchar(100) DEFAULT NULL COMMENT 'Complemento ou referência',
  `telefone` varchar(50) DEFAULT NULL COMMENT 'Telefone para contato',
  `situacao_de_residencia` int(1) NOT NULL DEFAULT '1',
  `alfabetizado` int(1) NOT NULL DEFAULT '1',
  `deficiencia` int(1) NOT NULL DEFAULT '0',
  `deficiencia_desc` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pessoa
-- ----------------------------
INSERT INTO `pessoa` VALUES ('1', '2018-06-29 13:23:34', '1', 'Igor Anthony Campos', '', '2000-05-27', '1', '5b363a6057a3d', '007.943.578-57', '447.737.636', 'MG', '', '', 'Enzo Noah César Campos', 'Gabrielly Carla Raquel', 'Beco Da Ligação', '360', '38', '', '(33)98401-7757', '0', '0', '0', '');
INSERT INTO `pessoa` VALUES ('2', '2018-06-29 13:25:51', '0', 'Melissa Lorena Santos', '', '2002-06-30', '2', '5b36368cad17c', '833.936.136-85', '453.584.676', 'MG', '', '', 'Davi José Moreira', 'Priscila Vitória', 'Rua Silvério Dutra', '70', '10', '', '(33)98334-0832', '1', '1', '1', 'dificuldade de locomocao');
INSERT INTO `pessoa` VALUES ('3', '2018-06-29 13:34:39', '0', 'Nicolas Sérgio Moreira', '', '1000-01-01', '0', '5b3636bb15c30', '498.585.236-88', '128.101.301', '', '', '', 'Jéssica Isabela', 'Jéssica Isabela', 'Avenida Miguel Arcanjo Brandão, S/n', '31', '21', '', '', '0', '0', '0', '');
INSERT INTO `pessoa` VALUES ('4', '2018-06-29 13:38:01', '1', 'Isabelle Aline De Paula', '', '2004-01-10', '2', '5b3636a42a608', '460.059.466-50', '278.079.799', 'MG', '', '', 'Carlos Sérgio Isaac De Paula', 'Bruna Joana', 'Rua Antonio Medeiros De Castro', '55', '5', 'fundos', '(33)99383-7482', '1', '1', '1', 'cegueira ');

-- ----------------------------
-- Table structure for profissao
-- ----------------------------
DROP TABLE IF EXISTS `profissao`;
CREATE TABLE `profissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da tabela',
  `profissao` varchar(45) NOT NULL COMMENT 'Nome da profissão',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profissao
-- ----------------------------
INSERT INTO `profissao` VALUES ('1', 'psicologo');
INSERT INTO `profissao` VALUES ('2', 'assistente social');

-- ----------------------------
-- Table structure for sessoes
-- ----------------------------
DROP TABLE IF EXISTS `sessoes`;
CREATE TABLE `sessoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária da Tabela',
  `cod_usuario` int(11) NOT NULL COMMENT 'chave estrangeira referenciando o usuario logado',
  `token` varchar(255) NOT NULL COMMENT 'token de sessão',
  `token_time` varchar(255) NOT NULL COMMENT 'data de expiração do token',
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'data de criação da sesão',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sessoes
-- ----------------------------
INSERT INTO `sessoes` VALUES ('1', '1', '5e467686cc4b0aa4bb9e1df4d5e1b89c1d0ac537', '1530284387', '2018-06-29 13:59:47');

-- ----------------------------
-- Table structure for sexo
-- ----------------------------
DROP TABLE IF EXISTS `sexo`;
CREATE TABLE `sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da tabela',
  `sexo` varchar(45) NOT NULL COMMENT 'nome do Sexo',
  `sigla_sexo` varchar(2) NOT NULL COMMENT 'Abreviação do nome do sexo',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sexo
-- ----------------------------

-- ----------------------------
-- Table structure for tipo_agendamento
-- ----------------------------
DROP TABLE IF EXISTS `tipo_agendamento`;
CREATE TABLE `tipo_agendamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_agendamento` varchar(45) NOT NULL,
  `cor_label` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_agendamento
-- ----------------------------

-- ----------------------------
-- Table structure for tipo_pessoa
-- ----------------------------
DROP TABLE IF EXISTS `tipo_pessoa`;
CREATE TABLE `tipo_pessoa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipo_pessoa
-- ----------------------------

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'chave primária da Tabela',
  `status` int(1) NOT NULL DEFAULT '1',
  `data_criacao` timestamp NULL DEFAULT NULL COMMENT 'data de criação do usuario',
  `nome` varchar(45) NOT NULL COMMENT 'nome de usuario',
  `email` varchar(45) DEFAULT NULL COMMENT 'email para contato',
  `senha` varchar(255) DEFAULT NULL COMMENT 'senha para acesso',
  `nascimento` date DEFAULT NULL COMMENT 'data de nascimento',
  `cod_profissao` int(11) DEFAULT NULL COMMENT 'Profissão exercida pelo usuário',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', '1', '2018-05-20 10:22:56', 'Administrador', 'admin@admin.com', '8e2e071fb7a1810bf8ec35a0158cd885027d5564', '2018-05-20', '1');
