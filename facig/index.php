<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

if (!isset($_SESSION['num'])){
    $_SESSION['num'] = rand(1, 99);
    while ($_SESSION['num']==22 or $_SESSION['num']==69 or $_SESSION['num']==43){
        $_SESSION['num'] = rand(1, 99);
    }
    $_SESSION['tent'] = 0;
}

if (isset($_POST['chute']) and $_POST['chute']!=""){
    if ($_SESSION['num'] == $_POST['chute']){
        $status="igual";
    }
    if ($_SESSION['num'] > $_POST['chute']){
        $status="maior";
    }
    if ($_SESSION['num'] < $_POST['chute']){
        $status="menor";
    }
    $_SESSION['tent']++;
}else{
    $status="nulo";
}


//reports
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);



?>
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <script src="js.js"></script>
    <link href="css.css" rel="stylesheet">
    <title>Grupo 2</title>
</head>
<body>
<main class="container">
    <div class="row">
        <div class="col-md-3">
            <br><br>
            <img src="cofre.png" alt="" class="float-left img-fluid">
        </div>
        <div class="col-md-5 text-center">
            <h3 class="text-danger">Ajude o Berranteiro</h3>
            <h5>Para proteger o sistema do CAF da <strong>onça</strong>,
                Serjão o guardou em seu cofre porém não lembra o último número da combinação.
                Tente decifrar o último número para acessar o sistema.
            </h5>
            <br>

                <div class="contem">
                    <div id="groupp">
                        <table class="table-bordered" id="tt">
                            <tr>
                                <td class="text-success">&nbsp;22&nbsp;</td>
                                <td class="text-success">&nbsp;69&nbsp;</td>
                                <td class="text-success">&nbsp;43&nbsp;</td>
                                <td class="text-danger">
                                    <div id="input"></div>
                                    <form action="" method="post">
                                        <input type="number" id="hiddenInput" maxlength="2" name="chute">
                                        <input type="submit" hidden>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
        </div>
        <div class="col-md-4">
            <img src="berr.png" alt="" class="float-right img-fluid">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 informacao">
            <?php
            if ($_SESSION['tent']>0){
                switch ($status){
                    case "igual":
                        echo "<h3 class='text-success'>Você acertou, o numero que faltava era o :{$_SESSION['num']}</h3>";
                        echo "<label for=''>Tentativas:{$_SESSION['tent']}</label>";
                        unset($_SESSION['num']);
                        unset($_SESSION['tent']);
                        echo "<h3 class='text-success'><img src='parabens.png' alt='' style='height: 100px;'>Berranteiro ira abrir o cofre e te direcionar para sistema em 5 segundos</h3>";
                        echo"<META HTTP-EQUIV=REFRESH CONTENT = '9;URL=http://159.89.42.146'>";
                        break;
                    case "maior":
                        echo "<h3 class='text-danger'>{$_POST['chute']}? Errou!! tente um maior <i class='fa fa-sort-amount-up'></i></h3>";
                        echo "<label for=''>Tentativas:{$_SESSION['tent']}</label>";
                        break;
                    case "menor":
                        echo "<h3 class='text-danger'>{$_POST['chute']}? Errou!! tente um menor <i class='fa fa-sort-amount-down'></i></h3>";
                        echo "<label for=''>Tentativas:{$_SESSION['tent']}</label>";
                        break;
                    default:
                        echo "<h3 class='text-warning'>ande logo, digita um numero e aperta enter</h3>";
                        echo "<label for=''>Tentativas:{$_SESSION['tent']}</label>";
                        break;
                }
            }
            ?>
        </div>
    </div>
</main>

</body>
</html>
<?php
//function primo($num){
//    $cont = 0;
//    for($i = 1; $i <= $num; $i++){
//        if($num % $i == 0){
//            $cont++;
//        }
//    }
//
//    if($cont==2){
//        return true;
//    }
//    else{
//        return false;
//    }
//}
//$num1 = rand(10, 1000);
//$resp = primo($num1);
//if ($resp == true) {
//    echo $num1." é um numero primo";
//}
//else {
//    echo $num1." não é um numero primo";
//}

?>



