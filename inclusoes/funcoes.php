<?php
//valida cpf
function validaCPF($cpf) {

    // Extrai somente os números
    $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

    // Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return 0;
    }
    // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return 0;
    }
    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return 0;
        }
    }
    return true;
}

///removetraçobarra
function limpadocumento($valor){
    $valor = trim($valor);
    $valor = str_replace(".", "", $valor);
    $valor = str_replace(",", "", $valor);
    $valor = str_replace("-", "", $valor);
    $valor = str_replace("/", "", $valor);
    $valor = str_replace("(", "", $valor);
    $valor = str_replace(")", "", $valor);
    return $valor;
}

///////////////////////////////////////////////////////////////////////////////////////
//converte a data para o formato utilizado pelo MySQL
// tambem testa se a data é valida, retorna 0 se for falsa
//data
function data2banco ($data) {
    if ( ! $data ) return 0;
    $data=explode('/',$data);
    if (isset($data[2]) && isset($data[1]) && isset($data[0]) && $data[2]>=1920){
        $data=$data[2].'-'.$data[1].'-'.$data[0];
        return $data;
    }else{
        return 0;
    }

}
//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA
function dataBanco2data ($data) {
    if ( ! $data ) return 0;
    $data=explode('-',$data);
    $data=$data[2].'/'.$data[1].'/'.$data[0];
    return $data;
}
//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA / HORA
function datahoraBanco2data ($data) {
    if ( ! $data ) return 0;
    $data=explode(' ',$data);
    $d=$data[0];
    $h=$data[1];
    $d=explode('-',$d);
    $d=$d[2].'/'.$d[1].'/'.$d[0].' ';
    $data=$d.$h;
    return $data;
}
//converte os dados do MYSQL para o formato utilizado pelo PDO/html
// DATA / date
function dataRetiraHora ($data) {
    if ( ! $data ) return 0;
    $data=explode(' ',$data);
    $d=$data[0];
    $d=explode('-',$d);
    $d=$d[2].'/'.$d[1].'/'.$d[0];
    $data=$d;
    return $data;
}
///////////////////////////////////////////////////////////////////////////////////////







///////////////////////////////////////////////////////////////////////////////////////
//função simples de calculo de idade
function Calculo_Idade($data) {
    //Data atual
    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');
    $nascimento = explode('-', $data);
    $dianasc = ($nascimento[2]);
    $mesnasc = ($nascimento[1]);
    $anonasc = ($nascimento[0]);
    //Calculando sua idade
    $idade = $ano - $anonasc; // simples, ano- nascimento!
    if ($mes < $mesnasc) // se o mes é menor, só subtrair da idade
    {
        $idade--;
        return $idade;
    }
    elseif ($mes == $mesnasc && $dia <= $dianasc) // se esta no mes do aniversario mas não passou ou chegou a data, subtrai da idade
    {
        $idade--;
        return $idade;
    }
    else // ja fez aniversario no ano, tudo certo!
    {
        return $idade;
    }
}
///////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Função para remover acentos de uma string
//function remover_caracter($string) {
//    $string = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
//    return $string;
//}
function remover_caracter($texto){
    $troca = [
        'á' => 'a',
        'à' => 'a',
        'ã' => 'a',
        'â' => 'a',
        'é' => 'e',
        'ê' => 'e',
        'í' => 'i',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ú' => 'u',
        'ü' => 'u',
        'Á' => 'A',
        'À' => 'A',
        'Ã' => 'A',
        'Â' => 'A',
        'É' => 'E',
        'Ê' => 'E',
        'Í' => 'I',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ú' => 'U',
        'Ü' => 'U',
        'ç' => 'c',
        'Ç' => 'C',
    ];
    return strtr($texto, $troca);
}
//////////////////////////////////////////////////////////////////////////////////////////////////




// retorna se já passou 24 horas
//retor 1 ainda nao passou
//retorna 0 já passou
function Expiradata($data,$periodo){
    //gera timestamp unix atual
    $now = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    //soma dias
    $dat = date('Y-m-d', strtotime("+{$periodo} days",strtotime($data)));
    $dd = explode("-", $dat);
    //gera timestamp unix
    $datacompara = mktime("08", "00", "01", $dd[1], $dd[2], $dd[0]);
    //compara se data atual é maior que atualização
    if ($now>$datacompara) {
        return 0;
    }else{
        return 1;
    }
}

function Expiradatahora($data,$periodo){
    //gera timestamp unix atual
    $now = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
    //soma dias
    $dat = date('Y-m-d-H-i-s', strtotime("+{$periodo} days",strtotime($data)));
    $dd = explode("-", $dat);
    //gera timestamp unix
    $datacompara = mktime($dd['3'], $dd['4'], $dd['5'], $dd[1], $dd[2], $dd[0]);
    //compara se data atual é maior que atualização
    if ($now>$datacompara) {
        return 0;
    }else{
        return 1;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
