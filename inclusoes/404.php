<?php
$page="Error 404-".$env->env_titulo;
$css = "404";
include_once("{$env->env_root}inclusoes/head.php");

echo"<META HTTP-EQUIV=REFRESH CONTENT = '15;URL={$env->env_url_mod}'>";
?>
<div class="container">
    <h1>Erro 404 <span>:(</span></h1>
    <p>Desculpe, mas a página que você estava tentando visualizar não existe.</p>
    <p>Parece que isso foi o resultado de:</p>
    <ul>
        <li>um endereço digitado errado</li>
        <li>um link desatualizado</li>
    </ul>
    <p><img src="<?php echo $env->env_estatico;?>img/berranteiro.png" style="height: 200px;" alt=""></p>
</div>
</body>
</html>
<script type="application/javascript">
    $(document).ready(function() {
        var audio = new Audio('<?php echo $env->env_estatico; ?>audio/coragem.mp3');
        audio.play();
    });
</script>