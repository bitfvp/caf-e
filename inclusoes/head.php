<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <META NAME="author" CONTENT="Works Huanderson Vitas Fabricio(matem todos)">
    <META NAME="description" CONTENT="Sistema de gestão do caf manhuaçu">
    <META NAME="keywords" CONTENT="caf manhuaçu ads facig ">

    <title><?php echo $page; ?></title>
    <?php include_once($env->env_root_mod . "inclusoes/style.php"); ?>
    <script>
        changeFavicon("<?php echo $env->env_url;?>fav.ico");
    </script>
</head>
<body>
<?php
if (isset($_SESSION['fsh']) and $_SESSION['fsh']!=null){
    $fsh=$_SESSION['fsh'];
    switch ($fsh['type']){
        case "success":
            $fsh['type']="green ";
            break;
        case "info":
            $fsh['type']="blue ";
            break;
        case "warning":
            $fsh['type']="yellow ";
            break;
        case "danger":
            $fsh['type']="red ";
            break;
        default:
            break;
    }
    ?>
    <script>
        $(document).ready(function(){
            var toastHTML = '<span><?php echo $fsh['flash'];?><br><?php if (isset($fsh['erro'])){echo $fsh['erro'];}?></span>';
            M.toast({html: toastHTML, classes: "<?php echo $fsh['type'];?> "});
        });
    </script>
    <?php
    if (isset($fsh['pointofview'])){ ?>
    <script>
        $(document).ready(function() {
            window.location.href='#pointofview';
        });
    </script>
    <?php }
    $_SESSION['fsh']=null;
    unset($_SESSION['fsh']);
} ?>
