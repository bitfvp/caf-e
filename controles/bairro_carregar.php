<?php
function fncbairrolist(){
    $sql = "SELECT * FROM bairro ORDER BY bairro";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $bairrolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $bairrolista;
}

function fncgetbairro($id){
    $sql = "SELECT * FROM bairro WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getbairro = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getbairro;
}
?>
