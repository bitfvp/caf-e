<?php
function fncprofissaolist(){
    $sql = "SELECT * FROM profissao ORDER BY profissao";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $profissaolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $profissaolista;
}

function fncgetprofissao($id){
    $sql = "SELECT * FROM profissao WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getprofissao = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getprofissao;
}
?>
