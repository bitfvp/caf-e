<?php
function fncpessoalist(){
    $sql = "SELECT * FROM pessoa ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $pessoalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $pessoalista;
}

function fncgetpessoa($id){
    $sql = "SELECT * FROM pessoa WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getpessoa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getpessoa;
}
?>
