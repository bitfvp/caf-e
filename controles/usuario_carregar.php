<?php
function fncusuariolist(){
    $sql = "SELECT * FROM usuario ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $usuariolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $usuariolista;
}

function fncgetusuario($id){
    $sql = "SELECT * FROM usuario WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getusuario = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getusuario;
}
?>
