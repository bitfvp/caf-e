<?php
function fncatividade_listalist(){
    $sql = "SELECT * FROM atividade_lista ORDER BY atividade";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $atividade_listalista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $atividade_listalista;
}

function fncgetatividade_lista($id){
    $sql = "SELECT * FROM atividade_lista WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getatividade_lista = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getatividade_lista;
}
?>
