<?php
function fnccontatolist(){
    $sql = "SELECT * FROM contato ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $contatolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $contatolista;
}

function fncgetcontato($id){
    $sql = "SELECT * FROM contato WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute();
    $getcontato = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcontato;
}
?>
