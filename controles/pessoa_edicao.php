<?php

if($startactiona==1 && $aca=="pessoasave"){
    $id=$_POST["id"];
    $nome = ucwords(strtolower($_POST['nome']));
    $nome_social = ucwords(strtolower($_POST['nome_social']));
    $nascimento = $_POST['nascimento'];
    $cod_sexo = $_POST['cod_sexo'];
    $cpf = $_POST['cpf'];
    $rg = $_POST['rg'];
    $rg_uf = strtoupper($_POST['rg_uf']);
    $cn = $_POST['cn'];
    $ctps = $_POST['ctps'];
    $pai = ucwords(strtolower($_POST['pai']));
    $mae = ucwords(strtolower($_POST['mae']));
    $endereco = ucwords(strtolower($_POST['endereco']));
    $numero = $_POST['numero'];
    $cod_bairro = $_POST['cod_bairro'];
    $referencia = $_POST['referencia'];
    $telefone = $_POST['telefone'];
    $situacao_de_residencia = $_POST['situacao_de_residencia'];
    $alfabetizado = $_POST['alfabetizado'];
    $deficiencia = $_POST['deficiencia'];
    $deficiencia_desc = $_POST['deficiencia_desc'];

    if($nascimento==""){
        $nascimento="1000-01-01";
    }
    if($numero==""){
        $numero=0;
    }

    if($cpf!=""){
        if (validaCPF($cpf)==0){
            $cpf_de_boa="nao_esta_de_boa";
        }else{
            $cpf_de_boa="de_boa";
        }

    }else{
        $cpf_de_boa="de_boa";
    }

    //
    if(empty($nome)){
        $_SESSION['fsh']=[
            "flash"=>"Preencha todos os campos!!",
            "type"=>"warning",
        ];

    }elseif ($cpf_de_boa!="de_boa"){
        $_SESSION['fsh']=[
            "flash"=>"CPF digitado é invalido!!",
            "type"=>"danger",
        ];
    } else{
        //executa classe cadastro
        $salvar= new Pessoa();
        $salvar->fncpessoaedit($id, $nome, $nome_social, $nascimento, $cod_sexo, $cpf, $rg, $rg_uf, $cn, $ctps, $pai, $mae, $endereco, $numero, $cod_bairro, $referencia, $telefone, $situacao_de_residencia, $alfabetizado, $deficiencia, $deficiencia_desc);
    }
}


///////////////////////////////////////////////////////////////////////////////////////////
if($startactiona==1 && $aca=="pessoaativar"){
    $id=$_GET["id"];


    if(empty($id)){
        $_SESSION['fsh']=[
            "flash"=>"Houve um erro, não há id!!",
            "type"=>"warning",
        ];

    }else{
        //executa classe cadastro
        $salvar= new Pessoa();
        $salvar->fncpessoaativar($id);
    }
}


///////////////////////////////////////////////////////
if($startactiona==1 && $aca=="cadastrosdesativar"){

    $salvar= new Pessoa();
    $salvar->fncpessoadesativar();
}

?>