$(document).ready(function(){
  $('.sidenav').sidenav();
});
$(document).ready(function(){
    $(".dropdown-trigger").dropdown();
});
$(document).ready(function(){
    $('select').formSelect();
});
$(document).ready(function(){
    $('.modal').modal();
});
$(document).ready(function(){
    $('.tooltipped').tooltip();

});
$(document).ready(function(){
    $('.parallax').parallax();
});
$(document).ready(function(){
    $('.materialboxed').materialbox();
});

function Mudarestado(el) {
  var display = document.getElementById(el).style.display;
  if(display == "none")
      document.getElementById(el).style.display = 'block';
  else
      document.getElementById(el).style.display = 'none';
}

function limite_textarea(valor,max) {
  quant = max;
  total = valor.length;
  if (total <= quant) {
      resto = quant - total;
      document.getElementById('cont').innerHTML = resto;
  } else {
      document.getElementById('descricao').value = valor.substr(0, quant);
  }
}

// adiciona via js um favicom
function changeFavicon(src) {
    var link = document.createElement('link'),
        oldLink = document.getElementById('dynamic-favicon');
    link.id = 'dynamic-favicon';
    link.rel = 'shortcut icon';
    link.href = src;
    if (oldLink) {
        document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
}