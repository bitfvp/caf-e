<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Busca ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

if (isset($_GET['search'])){
    $search = $_GET['search'];
    //consulta se ha busca
    $sql = "select * from contato WHERE nome LIKE '%$search%' ";
}else {
//consulta se nao ha busca
    $sql = "select * from contato ";
}
// total de registros a serem exibidos por página
$total_reg = "20"; // número de registros por página
//Se a página não for especificada a variável "pagina" tomará o valor 1, isso evita de exibir a página 0 de início
if (isset($_GET['pgn'])){
    $pgn=$_GET['pgn'];
}

if (!isset($pgn)) {
    $pc = "1";
} else {
    $pc = $pgn;
}
//Vamos determinar o valor inicial das buscas limitadas
$inicio = $pc - 1;
$inicio = $inicio * $total_reg;
//Vamos selecionar os dados e exibir a paginação
//limite
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $limite=$pdo->prepare($sql.$sql2);
    $limite->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
//todos
try{
    $sql2= "ORDER BY nome LIMIT $inicio,$total_reg";
    global $pdo;
    $todos=$pdo->prepare($sql);
    $todos->execute();
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
$tr=$todos->rowCount();// verifica o número total de registros
$tp = $tr / $total_reg; // verifica o número total de páginas

?>

    <div class="container">
        <h4>Contatos</h4>
        <hr>
        <div class="row">
            <div class="col s6">
                <form action="index.php" method="get">
                    <div class="input-field">
                        <input name="pg" value="Vcontato_lista" hidden/>
                        <input id="search" name="search" type="search" required autocomplete="off" placeholder="Pesquisar contato" value="<?php if (isset($_GET['search'])) {echo $_GET['search'];} ?>">
                        <input type="submit" hidden>
                        <script type="text/javascript">
                            function selecionaTexto() {
                                document.getElementById("search").select();
                            }
                            window.onload = selecionaTexto();
                        </script>
                        <i class="material-icons">close</i>
                    </div>
                </form>
            </div>

            <div class="col s6">
                <br>
                <a href="index.php?pg=Vcontato_editar" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Novo cadastro de contato">Novo</a>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                <table>
                    <thead>
                    <tr>
                        <th>Contato</th>
                        <th>Ações</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if(isset($_GET['search']) and $_GET['search']!="") {
                        $sta = strtoupper($_GET['search']);
                        define('CSA', $sta);//TESTE
                    }
                    // vamos criar a visualização
                    foreach ($limite as $dados){
                        $id = $dados["id"];
                        $nome = strtoupper($dados["nome"]);
                        if ($dados["nascimento"]=="1000-01-01"){
                            $nascimento = "--/--/----";
                        }else{
                            $nascimento = dataBanco2data($dados["nascimento"]);
                        }
                        $telefone = $dados["telefone"];
                        $endereco = $dados["endereco"];
                        $numero = $dados["numero"];
                        $bairro = $dados["bairro"];
                        $referencia = $dados["referencia"];
                        $cidade = $dados["cidade"];
                        $email = $dados["email"];

                        ?>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col s12">
                                        <?php
                                        if(isset($_GET['search']) and $_GET['search']!="") {
                                            $sta = CSA;
                                            $nnn = $nome;
                                            $nn = explode(CSA, $nnn);
                                            $n = implode("<span class='red-text'>{$sta}</span>", $nn);
                                            echo $n;
                                        }else{
                                            echo $nome;
                                        }
                                        ?>
                                    </div>
                                    <div class="col s4"><?php echo $nascimento;?></div>
                                    <div class="col s4"><?php echo $telefone;?></div>
                                    <div class="col s8"><?php echo $endereco." ".$numero;?></div>
                                    <div class="col s4"><?php echo $bairro;?></div>
                                    <div class="col s8"><?php echo $referencia;?></div>
                                    <div class="col s4"><?php echo $cidade;?></div>
                                    <div class="col s4"><?php echo $email;?></div>
                                </div>
                            </td>
                            <td>
                                <a class="waves-effect waves-light btn-small tooltipped" data-position="top" data-tooltip="Editar" href="index.php?pg=Vcontato_editar&id=<?php echo $id;?>">
                                    <i class="material-icons">create</i>
                                </a>
                            </td>
                        </tr>


                        <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col 6">
                <ul class="pagination">
                    <?php
                    $anterior = $pc -1;
                    $proximo = $pc +1;
                    if ($pc>1) {
                        echo " <li class='waves-effect'><a href='index.php?pg=Vcontato_lista&pgn={$anterior}&sca={$_GET['search']}'><i class='material-icons'>chevron_left</i></a></li> ";
                    }
                    echo "|";
                    if ($pc<$tp) {
                        echo " <li class='waves-effect'><a href='index.php?pg=Vcontato_lista&pgn={$proximo}&sca={$_GET['search']}' ><i class='material-icons'>chevron_right</i></a></li>";
                    }
                    ?>
                    </li>
                </ul>
            </div>

            <div class="col 6 right">
                <ul class="">
                    <li><?php echo $tr;?> Contato(s)</li>
                </ul>
            </div>
        </div>
    </div>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>