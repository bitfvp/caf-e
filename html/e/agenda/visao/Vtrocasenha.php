<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    //validação das permissoes
    //if ()
}
$page="Trocar senha ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>
    <div class="container">
        <div class="row">
            <div class="col s6 offset-s3">

                <!--card historico-->
                <div class="card light darken-1">
                    <div class="card-content blue-grey-text">
                        <div class="row">
                            <!--comeca form-->
                            <form action="?pg=Vtrocasenha&aca=novasenha" method="post">
                            <div class='row'>
                                <div class='row'>
                                    <div class='input-field col s8 offset-s2'>
                                        <input type='password' name='new_senha' id='new_senha' placeholder="Minimo 8 caracteres"/>
                                        <label for='new_senha'>Nova senha</label>
                                    </div>
                                </div>

                                <center>
                                    <div class='row'>
                                    <button type="submit" class='col s8 offset-s2 btn btn-large waves-effect waves-light indigo'>
                                        Salvar nova senha
                                    </button>
                                    </div>
                                </center>
                            </form>
                                <!--termina form-->
                            </div>
                        </div>
                    </div><!--fim do card historico-->

                </div><!-- fim da coluna central 12 -->
            </div>
        </div>

<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>