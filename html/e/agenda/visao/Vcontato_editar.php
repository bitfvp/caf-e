<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    //validação das permissoes
    //if ()
}
$page="Contato editar ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="contatosave";
    $contato=fncgetcontato($_GET['id']);
}else{
    $a="contatonew";
}
?>
<main class="container">
    <div class="row">
        <h4>Contato</h4>
        <hr>
    </div>

    <div class="row">
        <form action="<?php echo "index.php?pg=Vcontato_editar&aca={$a}"; ?>" method="post" class="col s12">

            <div class="row">
                <div class="col s6">
                    <br>
                    <input type="submit" value="Salvar" class="btn btn-block green col s12  waves-block waves-effect waves-light">
                </div>

                <div class="col s6">
                    <br>
                    <a href="index.php?pg=Vcontato_editar" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Abrir novo cadastro em branco">Novo cadastro</a>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input name="id"  id="id" type="hidden" value="<?php if (isset($contato['id'])){echo $contato['id'];} ?>"/>
                    <input name="nome" id="nome" type="text" class="validate" value="<?php if (isset($contato['nome'])){echo $contato['nome'];} ?>" required autocomplete="off">
                    <label for="nome">Nome</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s4">
                    <input name="nascimento" id="nascimento" type="date" value="<?php if (isset($contato['nascimento']) and $contato['nascimento']!="1000-01-01"){echo $contato['nascimento'];} ?>">
                    <label for="nascimento">Nascimento</label>
                </div>
                <div class="input-field col s4">
                    <input name="telefone" id="telefone" type="text" class="validate" value="<?php if (isset($contato['telefone'])){echo $contato['telefone'];} ?>" autocomplete="off">
                    <label for="telefone">Telefone</label>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input name="endereco" id="endereco" type="text" class="validate" value="<?php if (isset($contato['endereco'])){echo $contato['endereco'];} ?>" autocomplete="off">
                    <label for="endereco">Rua</label>
                </div>
                <div class="input-field col s2">
                    <input name="numero" id="numero" type="number" class="validate" value="<?php if (isset($contato['numero'])){echo $contato['numero'];} ?>" autocomplete="off">
                    <label for="numero">Numero</label>
                </div>
                <div class="input-field col s4">
                    <input name="bairro" id="bairro" type="text" class="validate" value="<?php if (isset($contato['bairro'])){echo $contato['bairro'];} ?>" autocomplete="off">
                    <label for="bairro">Bairro</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s5">
                    <input name="referencia" id="referencia" type="text" class="validate" value="<?php if (isset($contato['referencia'])){echo $contato['referencia'];} ?>" autocomplete="off">
                    <label for="referencia">Referencia</label>
                </div>
                <div class="input-field col s4">
                    <input name="cidade" id="cidade" type="text" class="validate" value="<?php if (isset($contato['cidade'])){echo $contato['cidade'];} ?>" autocomplete="off">
                    <label for="cidade">Cidade</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <input name="email" id="email" type="email" class="validate" value="<?php if (isset($contato['email'])){echo $contato['email'];} ?>" autocomplete="off">
                    <label for="email">E-mail</label>
                </div>
            </div>

        </form>
    </div>

</main>

<?php include_once("{$env->env_root}inclusoes/footer.php"); ?>
