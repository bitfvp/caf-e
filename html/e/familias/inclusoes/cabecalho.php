<div class="card-content blue-grey-text">
    <span class="card-title"><?php echo $pessoa['nome'];?></span>
    Sexo:<strong><?php echo fncgetsexo($pessoa['cod_sexo'])['sexo'] ;?></strong>
    Nascimento:<strong>
        <?php
        if ($pessoa['nascimento']!="1000-01-01"){
            echo dataBanco2data($pessoa['nascimento']);
            echo "<strong> ";
            echo Calculo_Idade($pessoa['nascimento'])." Ano(s)";
            echo "</strong>";
        }else{
            echo "NÃO DEFINIDO";
        }?>
    </strong>
    <br>
    CPF:<strong><?php echo $pessoa['cpf'];?></strong>
    RG:<strong><?php echo $pessoa['rg'];?></strong>
    uf:<strong><?php echo $pessoa['rg_uf'];?></strong>
    Certidão de nascimento:<strong><?php echo $pessoa['cn'];?></strong>
    Carteira de trabalho:<strong><?php echo $pessoa['ctps'];?></strong>
    <br>
    Pai:<strong><?php echo $pessoa['pai'];?></strong>
    Mae:<strong><?php echo $pessoa['mae'];?></strong>
    <br>
    endereço:<strong><?php echo $pessoa['endereco'];?></strong>
    numero:<strong><?php if ($pessoa['numero']==0){echo "S/N";}else{echo $pessoa['numero'];}?></strong>
    bairro:<strong><?php echo fncgetbairro($pessoa['cod_bairro'])['bairro'];?></strong>
    <br>
    Referencia:<strong><?php echo $pessoa['referencia'];?></strong>
    Telefone:<strong><?php echo $pessoa['telefone'];?></strong>
    Situação de residencia:<strong><?php
        if ($pessoa['situacao_de_residencia']==0){
            echo "Não declarada";
        }
        if ($pessoa['situacao_de_residencia']==1){
            echo "Propria";
        }
        if ($pessoa['situacao_de_residencia']==2){
            echo "Alugada";
        } ?></strong>
    Telefone:<strong><?php echo $pessoa['telefone'];?></strong>
    <br>
    Alfabetizado:<strong><?php
        if ($pessoa['alfabetizado']==0){
            echo "Não selecionado";
        }
        if ($pessoa['alfabetizado']==1){
            echo "SIM";
        }
        if ($pessoa['alfabetizado']==2){
            echo "NÃO";
        } ?></strong>
    Possui deficiência:<strong><?php
        if ($pessoa['deficiencia']==0){
            echo "Não selecionado";
        }
        if ($pessoa['deficiencia']==1){
            echo "SIM";
        }
        if ($pessoa['deficiencia']==2){
            echo "NÃO";
        }?></strong>

    <?php
    if ($pessoa['deficiencia']==1){
        echo "Descrição da deficiência:<strong>";
        echo $pessoa['deficiencia_desc'];
        echo "</desc>";
    }
    ?>
</div>
<div class="card-action">
    <a href="index.php?pg=Vpessoa_editar&id=<?php echo $_GET['id']?>" class="btn blue btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Alterar dados desse cadastro">Editar</a>
</div>