<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Atividades ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col s8">
            <div id="dados" class="card light darken-1" style="display: none;">
                <?php include_once("inclusoes/cabecalho.php");?>
            </div><!--fim do card dados-->

        <!--card form-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                    <form action="index.php?pg=Vat&aca=newatividade&id=<?php echo $_GET['id']; ?>" method="post" enctype="multipart/form-data">
                        <div class="input-field col s5">
                            <input name="data" id="data" type="date" required>
                            <label for="data">Data</label>
                        </div>

                        <div class="input-field col s7">
                            <select name="atividade" id="atividade" required>
                                <option value="" selected>Selecione </option>
                                <?php
                                foreach (fncatividade_listalist() as $item){
                                    echo "<option value='{$item['id']}'>{$item['atividade']}</option>";
                                }
                                ?>
                            </select>
                            <label for="Atividade">Atividade</label>
                        </div>

                        <div class="input-field col s12">
                            <textarea class="materialize-textarea" name="descricao" id="descricao" onkeyup="limite_textarea(this.value,10000)" maxlength="10000" rows="9" required></textarea>
                            <label for="descricao">Descrição</label>
                            <span id="cont">10000</span>/10000
                        </div>
                        <div class="input-field col s12">
                            <input name="arquivo" id="arquivo" type="file" accept="image/png, image/jpeg">
                        </div>

                        <div class="input-field col s12">
                            <input type="submit" class="btn btn-block btn-large green col s12 waves-block waves-effect waves-light" value="Salvar">
                        </div>
                    </form>
                </div>

            </div>
        </div><!--fim do card form-->


            <?php
            try {
            $sql = "SELECT * FROM atividade WHERE cod_pessoa=? "
                . "ORDER BY data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
            $atividade = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
            ?>
        <!--card historico-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                    <h4 id="pointofview">Prontuario de atendimentos</h4>
                    <?php
                    if ($cont!=0){
                        echo "<h6>{$cont} lançamento(s) encontrado(s)</h6><br>";
                        echo "<a href='index.php?pg=Vat_print&id={$_GET['id']}' target='_blank' class='btn tooltipped waves-block waves-effect waves-light' data-position='top' data-tooltip='Abre a tela com o prontuario de atendimentos para impressão'>Imprimir histórico</a>";
                    }else{
                        echo "<h5 class='red-text'>Não há historicos dessa pessoa</h5>";
                    }
                    foreach ($atividade as $at){
                    ?>
                    <blockquote>
                        Descrição: <strong>"<?php echo $at['descricao'];?>"</strong><br>
                        Atividade: <strong><?php echo fncgetatividade_lista($at['cod_atividade'])['atividade'];?></strong><br>
                        Data: <strong title="Lançado em <?php echo datahoraBanco2data($at['data_ts']);?>"><?php echo dataRetiraHora($at['data']);?></strong>
                        <span class="new badge red" data-badge-caption=""><?php echo $at['id'];?></span>
                        <footer>---
                            <?php echo fncgetusuario($at['cod_usuario'])['nome'];?>
                            (<?php echo fncgetprofissao(fncgetusuario($at['cod_usuario'])['cod_profissao'])['profissao'];?>)
                        </footer>
                    </blockquote>

                    <?php
                    if ($at['id'] != 0) {
                        $files = glob('../../arquivos/familias_at/' . $at['id'] . "/*.*");
                        for ($i = 0; $i < count($files); $i++) {
                            $num = $files[$i];
                            $extencao = explode(".", $num);
                            //ultima posicao do array
                            $ultimo = end($extencao);
                            switch ($ultimo) {
                                case "docx":
                                    echo "<div class='row'>";
                                    echo "<div class='col s6'>";
                                    echo "<a href=" . $num . " target='_blank'>";
                                    echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='responsive-img'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                    break;

                                case "doc":
                                    echo "<div class='row'>";
                                    echo "<div class='col s6'>";
                                    echo "<a href=" . $num . " target='_blank'>";
                                    echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='responsive-img'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                    break;

                                case "xls":
                                    echo "<div class='row'>";
                                    echo "<div class='col s6'>";
                                    echo "<a href=" . $num . " target='_blank'>";
                                    echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='responsive-img'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                    break;

                                case "xlsx":
                                    echo "<div class='row'>";
                                    echo "<div class='col s6'>";
                                    echo "<a href=" . $num . " target='_blank'>";
                                    echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='responsive-img'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                    break;

                                case "pdf":
                                    echo "<div class='row'>";
                                    echo "<div class='col s6'>";
                                    echo "<a href=" . $num . " target='_blank'>";
                                    echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='responsive-img'>";
                                    echo "</a>";
                                    echo "</div>";
                                    echo "</div>";
                                    break;

                                default:
                                    echo "<div class='row'>\n";
                                    echo "<div class='col s6' >\n";
                                    echo "<a href=". $num ." target='_blank'>\n";
                                    echo "<img src=" . $num . " class='responsive-img'>\n";
                                    echo "</a>\n";
                                    echo "</div>\n";
                                    echo "</div>\n";
                                    break;
                            }

                        }
                    }//fim de foto
                    ?>


                    <?php } ?>
                </div>
            </div>
        </div><!--fim do card historico-->

    </div><!-- fim da coluna central 8 -->

    <button type="button" onclick="Mudarestado('dados')" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Mostrar dados dessa pessoa">Mostrar dados</button>
        <?php
        include_once("inclusoes/menu_lateral.php")
        ?>

    </div>
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>