<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    //validação das permissoes
    //if ()
}
$page="Pessoa editar ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="pessoasave&id=".$_GET['id'];
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $a="pessoanew";
}
?>
<main class="container">
    <div class="row">
        <h4>Pessoa</h4>
        <hr>
    </div>

    <div class="row">
        <form action="<?php echo "index.php?pg=Vpessoa_editar&aca={$a}"; ?>" method="post" class="col s12">

            <div class="row">
                <div class="col s6">
                    <br>
                    <input type="submit" value="Salvar" class="btn btn-block green col s12  waves-block waves-effect waves-light">
                </div>

                <div class="col s6">
                    <br>
                    <a href="index.php?pg=Vpessoa_editar" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Abrir novo cadastro em branco">Novo cadastro</a>
                </div>
            </div>

            <div class="row">
                <h4>Dados</h4>
                <div class="input-field col s4">
                    <input name="id"  id="id" type="hidden" value="<?php if (isset($pessoa['id'])){echo $pessoa['id'];} ?>"/>
                    <input name="nome" id="nome" type="text" class="validate" value="<?php if (isset($pessoa['nome'])){echo $pessoa['nome'];} ?>" required autocomplete="off">
                    <label for="nome">Nome</label>
                </div>
                <div class="input-field col s3">
                    <input name="nome_social" id="nome_social" type="text" class="validate" value="<?php if (isset($pessoa['nome_social'])){echo $pessoa['nome_social'];} ?>" autocomplete="off">
                    <label for="nome_social">Nome Social</label>
                </div>
                <div class="input-field col s3">
                    <input name="nascimento" id="nascimento" type="date" value="<?php if (isset($pessoa['nascimento']) and $pessoa['nascimento']!="1000-01-01"){echo $pessoa['nascimento'];} ?>">
                    <label for="nascimento">Nascimento</label>
                </div>
                <div class="input-field col s2">
                    <select name="cod_sexo" class="validate" required="true">
                        <option selected="" value="<?php if (isset($pessoa['cod_sexo']) and $pessoa['cod_sexo']<4 and $pessoa['cod_sexo']>0){echo $pessoa['cod_sexo'];}else{echo 0;} ?>"><?php
                            if (isset($pessoa['cod_sexo'])){
                                if ($pessoa['cod_sexo']==0){
                                    echo "Selecione";
                                }
                                if ($pessoa['cod_sexo']==1){
                                    echo "MASCULINO";
                                }
                                if ($pessoa['cod_sexo']==2){
                                    echo "FEMININO";
                                }
                                if ($pessoa['cod_sexo']==3){
                                    echo "INDEFINIDO";
                                }
                            }else{
                                echo "Selecione";
                            }
                            ?></option>
                        <option value="1">Homem</option>
                        <option value="2">Mulher</option>
                        <option value="3">Indefinido</option>
                    </select>
                    <label>Sexo</label>
                </div>
            </div>

                <div class="row">
                    <div class="input-field col s5">
                        <input name="cpf" id="cpf" type="text" class="validate" value="<?php if (isset($pessoa['cpf'])){echo $pessoa['cpf'];} ?>" autocomplete="off">
                        <label for="cpf">CPF</label>
                    </div>
                    <script>
                        $(document).ready(function(){
                            $('#cpf').mask('000.000.000-00', {reverse: false});
                        });
                    </script>
                    <div class="input-field col s5">
                        <input name="rg" id="rg" type="text" class="validate" value="<?php if (isset($pessoa['rg'])){echo $pessoa['rg'];} ?>" autocomplete="off">
                        <label for="rg">RG</label>
                    </div>
                    <script>
                        $(document).ready(function(){
                            $('#rg').mask('00.000.000.000', {reverse: true});
                        });
                    </script>
                    <div class="input-field col s2">
                        <input name="rg_uf" id="rg_uf" type="text" class="validate" value="<?php if (isset($pessoa['rg_uf'])){echo $pessoa['rg_uf'];} ?>" autocomplete="off">
                        <label for="rg_uf">RG-UF</label>
                    </div>
                </div>

            <div class="row">
                <div class="input-field col s6">
                    <input name="cn" id="cn" type="text" class="validate" value="<?php if (isset($pessoa['cn'])){echo $pessoa['cn'];} ?>" autocomplete="off">
                    <label for="cn">Certidao de nascimento</label>
                </div>
                <div class="input-field col s6">
                    <input name="ctps" id="ctps" type="text" class="validate"  value="<?php if (isset($pessoa['ctps'])){echo $pessoa['ctps'];} ?>" autocomplete="off">
                    <label for="ctps">CTPS</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input name="pai" id="pai" type="text" class="validate" value="<?php if (isset($pessoa['pai'])){echo $pessoa['mae'];} ?>" autocomplete="off">
                    <label for="pai">Pai</label>
                </div>
                <div class="input-field col s6">
                    <input name="mae" id="mae" type="text" class="validate" value="<?php if (isset($pessoa['mae'])){echo $pessoa['mae'];} ?>" autocomplete="off">
                    <label for="mae">Mae</label>
                </div>

            </div>

            <div class="row">
                <h4>Endereço</h4>
                <div class="input-field col s6">
                    <input name="endereco" id="endereco" type="text" class="validate" value="<?php if (isset($pessoa['endereco'])){echo $pessoa['endereco'];} ?>" autocomplete="off">
                    <label for="endereco">Rua</label>
                </div>
                <div class="input-field col s2">
                    <input name="numero" id="numero" type="number" class="validate" value="<?php if (isset($pessoa['numero'])){echo $pessoa['numero'];} ?>" autocomplete="off">
                    <label for="numero">Numero</label>
                </div>
                <div class="input-field col s4">
                    <select name="cod_bairro" class="validate" required="true">
                        <option selected="" value="<?php if (isset($pessoa['cod_bairro'])){echo $pessoa['cod_bairro'];}else{echo 0;}?>">
                            <?php
                            if (isset($pessoa['cod_bairro']) and $pessoa['cod_bairro']!=""){
                                $cod_bairroid=$pessoa['cod_bairro'];
                                $bairro=fncgetbairro($cod_bairroid);
                                echo $bairro['bairro'];
                            }else{
                                echo "Selecione";}
                            ?>
                        </option>
                        <?php
                        foreach(fncbairrolist() as $item){?>
                            <option value="<?php echo $item['id']; ?>"><?php echo $item['bairro']; ?></option>
                        <?php } ?>
                    </select>
                    <label for="bairro">Bairro</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input name="referencia" id="referencia" type="text" class="validate" value="<?php if (isset($pessoa['referencia'])){echo $pessoa['referencia'];} ?>" autocomplete="off">
                    <label for="referencia">Referencia</label>
                </div>
                <div class="input-field col s6">
                    <input name="telefone" id="telefone" type="text" class="validate" value="<?php if (isset($pessoa['telefone'])){echo $pessoa['telefone'];} ?>" autocomplete="off">
                    <label for="telefone">Telefone</label>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>

            <div class="row">
                <h4>Outros</h4>
                <div class="input-field col s6">
                    <select name="situacao_de_residencia" required="true">
                        <option selected="" value="<?php if (isset($pessoa['situacao_de_residencia']) and $pessoa['situacao_de_residencia']>0 and $pessoa['situacao_de_residencia']<3){echo $pessoa['situacao_de_residencia'];}else{echo 0;} ?>">
                            <?php
                            if (isset($pessoa['situacao_de_residencia'])){
                                if ($pessoa['situacao_de_residencia']==0){
                                    echo "Selecione";
                                }
                                if ($pessoa['situacao_de_residencia']==1){
                                    echo "Propria";
                                }
                                if ($pessoa['situacao_de_residencia']==2){
                                    echo "Alugada";
                                }
                            }else{
                                echo "Selecione";
                            }
                            ?>
                        </option>
                        <option value="1">Propria </option>
                        <option value="2">Alugada</option>
                    </select>
                    <label for="tipo_de_residencia">Tipo de Residencia</label>
                </div>
                <div class="input-field col s6">
                    <select name="alfabetizado" required="true">
                        <option selected="" value="<?php if (isset($pessoa['alfabetizado']) and $pessoa['alfabetizado']>0 and $pessoa['alfabetizado']<3){echo $pessoa['alfabetizado'];}else{echo 0;} ?>">
                            <?php
                            if (isset($pessoa['alfabetizado'])){
                                if ($pessoa['alfabetizado']==0){
                                    echo "Selecione";
                                }
                                if ($pessoa['alfabetizado']==1){
                                    echo "SIM";
                                }
                                if ($pessoa['alfabetizado']==2){
                                    echo "NÃO";
                                }
                            }else{
                                echo "Selecione";
                            }
                            ?>
                        </option>
                        <option value="2">NÂO </option>
                        <option value="1">SIM </option>
                    </select>
                    <label for="alfabetizado">Alfabetizado</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <select name="deficiencia" required="true">
                        <option selected="" value="<?php if (isset($pessoa['deficiencia']) and $pessoa['deficiencia']>0 and $pessoa['deficiencia']<3){echo $pessoa['deficiencia'];}else{echo 0;} ?>">
                            <?php
                            if (isset($pessoa['deficiencia'])){
                                if ($pessoa['deficiencia']==1){
                                    echo "SIM";
                                }
                                if ($pessoa['deficiencia']==2){
                                    echo "NÃO";
                                }
                            }else{
                                echo "Selecione";
                            }
                            ?>
                        </option>
                        <option value="2">NÂO </option>
                        <option value="1">SIM </option>
                    </select>
                    <label for="deficiencia">Possui deficiência</label>
                </div>


                <div class="input-field col s6">
                    <input name="deficiencia_desc" id="deficiencia_desc" type="text" class="validate" value="<?php if (isset($pessoa['deficiencia_desc'])){echo $pessoa['deficiencia_desc'];} ?>" autocomplete="off">
                    <label for="deficiencia_desc">Descrição da deficiência</label>
                </div>
            </div>

        </form>
    </div>

</main>

<?php include_once("{$env->env_root}inclusoes/footer.php"); ?>
