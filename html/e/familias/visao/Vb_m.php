<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Minhas Atividades ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>
<main class="container">
    <div class="row">

        <div class="col s12">


            <?php
            try {
            $sql = "SELECT * FROM beneficio WHERE cod_usuario=? "
                . "ORDER BY data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_SESSION['id']);
            $consulta->execute();
            $beneficios = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
            ?>
        <!--card historico-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                    <h4>Todos os benefícios lançados por mim</h4>
                    <?php
                    if ($cont!=0){
                        echo "<h6>{$cont} lançamento(s) encontrado(s)</h6><br>";
                    }else{
                        echo "<h5 class='red-text'>Você ainda não tem lançamentos</h5>";
                    }
                    foreach ($beneficios as $ben){
                    ?>
                    <blockquote>
                        <h5><a href="index.php?pg=Vat&id=<?php echo $ben['cod_pessoa'];?>"><?php echo fncgetpessoa($ben['cod_pessoa'])['nome'];?></a></h5><br>
                        Beneficio: <strong><?php echo fncgetbeneficio_lista($ben['cod_beneficio'])['beneficio']?></strong><br>
                        Quantidade: <strong><?php echo $ben['quantidade'];?></strong><br>
                        Data: <strong title="Lançado em <?php echo datahoraBanco2data($ben['data_ts']);?>"><?php echo dataRetiraHora($ben['data']);?></strong><br>
                        Descrição: <strong>"<?php echo $ben['descricao'];?>"</strong><br>
                    </blockquote>
                    <?php } ?>
                </div>
            </div>
        </div><!--fim do card historico-->

    </div><!-- fim da coluna central 8 -->
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>