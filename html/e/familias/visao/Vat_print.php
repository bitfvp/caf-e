<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Prontuario ".$env->env_titulo;
$css="print";
include_once("{$env->env_root}inclusoes/head.php");


if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<style media=all>
    @media print {
        @page {
            margin: 0.59cm auto;
        }
    }
</style>
<main class="container">

    <h3><?php echo $pessoa['nome'];?></h3>
    Sexo:<strong><?php echo fncgetsexo($pessoa['cod_sexo'])['sexo'] ;?></strong>
    Nascimento:<strong>
        <?php
        if ($pessoa['nascimento']!="1000-01-01"){
            echo dataBanco2data($pessoa['nascimento']);
            echo "<strong> ";
            echo Calculo_Idade($pessoa['nascimento'])." Ano(s)";
            echo "</strong>";
        }else{
            echo "NÃO DEFINIDO";
        }?>
    </strong>
    <br>
    CPF:<strong><?php echo $pessoa['cpf'];?></strong>
    RG:<strong><?php echo $pessoa['rg'];?></strong>
    uf:<strong><?php echo $pessoa['rg_uf'];?></strong>
    Certidão de nascimento:<strong><?php echo $pessoa['cn'];?></strong>
    Carteira de trabalho:<strong><?php echo $pessoa['ctps'];?></strong>
    <br>
    Pai:<strong><?php echo $pessoa['pai'];?></strong>
    Mae:<strong><?php echo $pessoa['mae'];?></strong>
    <br>
    endereço:<strong><?php echo $pessoa['endereco'];?></strong>
    numero:<strong><?php if ($pessoa['numero']==0){echo "S/N";}else{echo $pessoa['numero'];}?></strong>
    bairro:<strong><?php echo fncgetbairro($pessoa['cod_bairro'])['bairro'];?></strong>
    <br>
    Referencia:<strong><?php echo $pessoa['referencia'];?></strong>
    Telefone:<strong><?php echo $pessoa['telefone'];?></strong>
    Situação de residencia:<strong><?php
        if ($pessoa['situacao_de_residencia']==0){
            echo "Não declarada";
        }
        if ($pessoa['situacao_de_residencia']==1){
            echo "Propria";
        }
        if ($pessoa['situacao_de_residencia']==2){
            echo "Alugada";
        } ?></strong>
    Telefone:<strong><?php echo $pessoa['telefone'];?></strong>
    <br>
    Alfabetizado:<strong><?php
        if ($pessoa['alfabetizado']==0){
            echo "Não selecionado";
        }
        if ($pessoa['alfabetizado']==1){
            echo "SIM";
        }
        if ($pessoa['alfabetizado']==2){
            echo "NÃO";
        } ?></strong>
    Possui deficiência:<strong><?php
        if ($pessoa['deficiencia']==0){
            echo "Não selecionado";
        }
        if ($pessoa['deficiencia']==1){
            echo "SIM";
        }
        if ($pessoa['deficiencia']==2){
            echo "NÃO";
        }?></strong>

    <?php
    if ($pessoa['deficiencia']==1){
        echo "Descrição da deficiência:<strong>";
        echo $pessoa['deficiencia_desc'];
        echo "</desc>";
    }
    ?>

    <hr>
            <?php
            try {
            $sql = "SELECT * FROM atividade WHERE cod_pessoa=? "
                . "ORDER BY data ASC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
            $atividade = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }


                    if ($cont!=0){
                        echo "<h6>{$cont} atividade(s) encontrada(s)</h6><br>";
                    }else{
                        echo "<h5 class='red-text'>Não há historicos dessa pessoa</h5>";
                    }
                    foreach ($atividade as $at){
                    ?>
                    <blockquote>
                        Descrição: <strong>"<?php echo $at['descricao'];?>"</strong><br>
                        Atividade: <strong><?php echo fncgetatividade_lista($at['cod_atividade'])['atividade'];?></strong><br>
                        Data: <strong title="Lançado em <?php echo datahoraBanco2data($at['data_ts']);?>"><?php echo dataRetiraHora($at['data']);?></strong>
                        <footer>---
                            <?php echo fncgetusuario($at['cod_usuario'])['nome'];?>
                            (<?php echo fncgetprofissao(fncgetusuario($at['cod_usuario'])['cod_profissao'])['profissao'];?>)
                        </footer>
                    </blockquote>
                    <?php } ?>

</main>
</body>
</html>