<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Atividades Relatório ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>
    <main class="container">

        <div class="row">
            <h4>Relatório de Atividades</h4>
            <hr>
            <div class="col s6 offset-s3">
                <!--card historico-->
                <div class="card light darken-1">
                    <div class="card-content blue-grey-text">
                        <div class="row">
                            <!--comeca form-->
                            <form action="index.php?pg=Vat_r_print" method="post" target="_blank">
                            <div class='row'>
                                <div class='input-field col s8 offset-s2'>
                                    <input type='date' name='data_inicio' id='data_inicio' required/>
                                    <label for='data_inicio'>Inicio</label>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='input-field col s8 offset-s2'>
                                    <input type='date' name='data_final' id='data_final' required/>
                                    <label for='data_final'>Final</label>
                                </div>
                            </div>
                            <br />
                            <center>
                                <div class='row'>
                                    <button type="submit" class='col s8 offset-s2 btn btn-large waves-effect waves-light purple'>
                                        Emitir Relatório
                                    </button>
                                </div>
                            </center>
                            </form>
                            <!--termina form-->
                        </div>
                    </div>
                </div><!--fim do card historico-->

            </div><!-- fim da coluna central 12 -->
        </div>

    </main>
<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>