<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Atividades Relatório ".$env->env_titulo;
$css="print";
include_once("{$env->env_root}inclusoes/head.php");

// Recebe
$inicial=$_POST['data_inicio'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT "
    ."atividade.id, "
    ."usuario.nome AS profissional, "
    ."pessoa.nome AS pessoa, "
    ."atividade_lista.atividade,atividade.`data` "
    ."FROM "
    ."atividade "
    ."INNER JOIN atividade_lista ON atividade_lista.id = atividade.cod_atividade "
    ."INNER JOIN usuario ON usuario.id = atividade.cod_usuario "
    ."INNER JOIN pessoa ON pessoa.id = atividade.cod_pessoa "
    ."WHERE "
    ."atividade.`data` >= :inicial AND "
    ."atividade.`data` <= :final "
    ."ORDER BY "
    ."profissional ASC, "
    ."atividade_lista.atividade ASC ";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->execute();
$ati = $consulta->fetchAll();
$sql=null;
$consulta=null;

?>
<style media=all>
    .table-sm {
        font-size:10px !important;
        widows: 2;
        width: 100%;
    }
    @media print {
        @page {
            margin: 0.59cm auto;
        }
    }
</style>
<div class="container-fluid">
    <h1>Relatório de Atividades</h1>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicio'])." a ".dataBanco2data($_POST['data_final']);?></h5>
    <?php
    $acont=0;
    $a=$ppp="a";
    $tcont=0;
    echo "<table class='table table-striped table-bordered table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>Pessoa</td>";
    echo "<td>Atividade</td>";
    echo "<td>Data</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach($ati as $at){

        if(((isset($ppp))and ($ppp!=$at['profissional']) and ($acont>0))or((isset($a))and ($a!=$at['atividade'])) and ($acont>0)){

            echo "<tr>";
            echo '<td colspan="4" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
            echo "</tr>";
            $acont=0;
            if ((isset($ppp))and ($ppp!=$at['profissional'])){
                echo "<tr>";
                echo '<td colspan="4" class=\'text-right font-weight-bold\'>Total '.$tcont.'</td>';
                echo "</tr>";
                $tcont=0;
            }

        }
        if(((isset($ppp))and ($ppp!=$at['profissional']))or((isset($a))and ($a!=$at['atividade']))){

            echo "<tr>";
            echo "<td colspan='4' class='text-center font-weight-bold'>";
            echo $at['profissional']." - ".$at['atividade'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo $at['atividade']; ?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $tcont++;
        $ppp=$at['profissional'];
        $a=$at['atividade'];
    }
    echo "<tr>";
    echo '<td colspan="4" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
    echo "</tr>";
    $acont=0;
    echo "<tr>";
    echo '<td colspan="4" class=\'text-right font-weight-bold\'>Total '.$tcont.'</td>';
    echo "</tr>";
    $tcont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
<fieldset>
    <div id="piechart" style="width: auto; height: 900px;"></div>
</fieldset>
</body>
</html>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- //////////////////////////////////////////////////////////////////:: -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        //montando o array com os dados
        var data = google.visualization.arrayToDataTable([
            ['Atividade', 'Quant'],
            <?php
            // Recebe
            $inicial = $_POST['data_inicio']." 00:00:01";
            $final = $_POST['data_final']." 23:59:59";

            $sql = "SELECT "
                ."Count(atividade.id) AS contadora, "
                . "atividade_lista.atividade AS atv "
                . "FROM "
                . "atividade "
                . "INNER JOIN atividade_lista ON atividade_lista.id = atividade.cod_atividade "
                . "WHERE "
                . "atividade.`data` >= :inicial AND "
                . "atividade.`data` <= :final "
                . "GROUP BY "
                . "atividade.cod_atividade "
                . "ORDER BY "
                . "contadora DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindValue(":inicial", $inicial);
                $consulta->bindValue(":final", $final);
                $consulta->execute();
                $graf = $consulta->fetchAll();
                $sql = null;
                $consulta = null;


            foreach ($graf as $gf) {
                echo '[\''.$gf['contadora'].' '.$gf['atv'].'\',  '.$gf['contadora'].'],'."\n";
            }
            ?>
        ]);



        //opções para o gráfico pizza
        var options3 = {
            title: 'Grafico torta de atividades por período',
            is3D: true,
            //sliceVisibilityThreshold : .01
        };
        //instanciando e desenhando para o gráfico pizza
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options3);
    }
</script>