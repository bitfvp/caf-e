<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Benefícios Relatório ".$env->env_titulo;
$css="print";
include_once("{$env->env_root}inclusoes/head.php");

// Recebe
$inicial=$_POST['data_inicio'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

$sql = "SELECT "
    ."beneficio.id, "
    ."usuario.nome AS profissional, "
    ."pessoa.nome AS pessoa, "
    ."beneficio_lista.beneficio, "
    ."beneficio.`data`, "
    ."beneficio.quantidade "
    ."FROM "
    ."beneficio "
    ."INNER JOIN beneficio_lista ON beneficio_lista.id = beneficio.cod_beneficio "
    ."INNER JOIN usuario ON usuario.id = beneficio.cod_usuario "
    ."INNER JOIN pessoa ON pessoa.id = beneficio.cod_pessoa "
    ."WHERE "
    ."beneficio.`data` >= :inicial AND "
    ."beneficio.`data` <= :final "
    ."ORDER BY "
    ."beneficio_lista.beneficio ASC, "
    ."beneficio.`data` ASC";
global $pdo;
$consulta = $pdo->prepare($sql);
$consulta->bindValue(":inicial",$inicial);
$consulta->bindValue(":final",$final);
$consulta->execute();
$ati = $consulta->fetchAll();
$sql=null;
$consulta=null;

?>
<style media=all>
    .table-sm {
        font-size:10px !important;
        widows: 2;
        width: 100%;
    }
    @media print {
        @page {
            margin: 0.59cm auto;
        }
    }
</style>
<div class="container-fluid">
    <h1>Relatório de Benefícios</h1>
    <h5>Período:<?php echo dataBanco2data($_POST['data_inicio'])." a ".dataBanco2data($_POST['data_final']);?></h5>
    <?php
    $acont=0;
    $a="a";
    echo "<table class='table table-striped table-bordered table-sm'>";
    echo "<thead class='thead-default'>";
    echo "<tr>";
    echo "<td>Beneficiario</td>";
    echo "<td>Quantidade</td>";
    echo "<td>Data</td>";
    echo "<td>Profissional</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    foreach($ati as $at){

        if($a!=$at['beneficio'] and $acont>0){

            echo "<tr>";
            echo '<td colspan="5" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
            echo "</tr>";
            $acont=0;

        }
        if((isset($a) and ($a!=$at['beneficio']))){

            echo "<tr>";
            echo "<td colspan='5' class='text-center font-weight-bold'>";
            echo $at['beneficio'];
            echo "</td>";
            echo "</tr>";

        }
        ?>

        <tr>
            <td><?php echo $at['pessoa'];?>&nbsp;</td>
            <td><?php echo $at['quantidade']; ?>&nbsp;</td>
            <td><?php echo dataRetiraHora($at['data']); ?>&nbsp;</td>
            <td><?php echo $at['profissional']; ?>&nbsp;</td>
        </tr>

        <?php
        $acont++;
        $ppp=$at['profissional'];
        $a=$at['beneficio'];
    }
    echo "<tr>";
    echo '<td colspan="5" class=\'text-right font-weight-bold\'>subtotal '.$acont.'</td>';
    echo "</tr>";
    $acont=0;
    echo '</tbody>';
    echo '</table>';
    $acont=0;
    ?>


</div>
</body>
</html>