<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Minhas Atividades ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>
<main class="container">
    <div class="row">

        <div class="col s12">


            <?php
            try {
            $sql = "SELECT * FROM atividade WHERE cod_usuario=? "
                . "ORDER BY data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_SESSION['id']);
            $consulta->execute();
            $atividade = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
            ?>
        <!--card historico-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                        <h4>Todos os meus atendimentos</h4>
                        <?php
                        if ($cont!=0){
                            echo "<h6>{$cont} lançamento(s) encontrado(s)</h6><br>";
                        }else{
                            echo "<h5 class='red-text'>Você ainda não tem lançamentos</h5>";
                        }
                        foreach ($atividade as $at){
                        ?>
                    <div class="row">
                        <div class="col s7">
                            <blockquote>
                                <h5><a href="index.php?pg=Vat&id=<?php echo $at['cod_pessoa'];?>"><?php echo fncgetpessoa($at['cod_pessoa'])['nome'];?></a></h5><br>
                                Descrição: <strong>"<?php echo $at['descricao'];?>"</strong><br>
                                Atividade: <strong><?php echo fncgetatividade_lista($at['cod_atividade'])['atividade'];?></strong><br>
                                Data: <strong title="Lançado em <?php echo datahoraBanco2data($at['data_ts']);?>"><?php echo dataRetiraHora($at['data']);?></strong>
                                <span class="new badge red" data-badge-caption=""><?php echo $at['id'];?></span>
                            </blockquote>
                        </div>
                        <div class="col s5">
                            <?php
                            if ($at['id'] != 0) {
                                $files = glob('../../arquivos/familias_at/' . $at['id'] . "/*.*");
                                for ($i = 0; $i < count($files); $i++) {
                                    $num = $files[$i];
                                    $extencao = explode(".", $num);
                                    //ultima posicao do array
                                    $ultimo = end($extencao);
                                    switch ($ultimo) {
                                        case "docx":
                                            echo "<div class='row'>";
                                            echo "<div class='col s6'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/docx.png alt='...' class='responsive-img'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "doc":
                                            echo "<div class='row'>";
                                            echo "<div class='col s6'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/doc.png alt='...' class='responsive-img'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xls":
                                            echo "<div class='row'>";
                                            echo "<div class='col s6'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='responsive-img'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "xlsx":
                                            echo "<div class='row'>";
                                            echo "<div class='col s6'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/xls.png alt='...' class='responsive-img'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        case "pdf":
                                            echo "<div class='row'>";
                                            echo "<div class='col s6'>";
                                            echo "<a href=" . $num . " target='_blank'>";
                                            echo "<img src=" . $env->env_estatico . "img/pdf.png alt='...' class='responsive-img'>";
                                            echo "</a>";
                                            echo "</div>";
                                            echo "</div>";
                                            break;

                                        default:
                                            echo "<div class='row'>\n";
                                            echo "<div class='col s6' >\n";
                                            echo "<a href=". $num ." target='_blank'>\n";
                                            echo "<img src=" . $num . " class='responsive-img'>\n";
                                            echo "</a>\n";
                                            echo "</div>\n";
                                            echo "</div>\n";
                                            break;
                                    }

                                }
                            }//fim de foto
                            ?>
                        </div>
                    </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div><!--fim do card historico-->

    </div><!-- fim da coluna central 8 -->
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>