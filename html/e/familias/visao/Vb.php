<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Benefícios ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col s8">
            <div id="dados" class="card light darken-1" style="display: none;">
                <?php include_once("inclusoes/cabecalho.php");?>
            </div><!--fim do card dados-->

        <!--card form-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                    <form action="index.php?pg=Vb&aca=newbeneficio&id=<?php echo $_GET['id']; ?>" method="post">
                        <div class="input-field col s5">
                            <input name="data" id="data" type="date" required>
                            <label for="data">Data</label>
                        </div>

                        <div class="input-field col s7">
                            <select name="beneficio" id="beneficio" required>
                                <option value="" selected>Selecione </option>
                                <?php
                                foreach (fncbeneficio_listalist() as $item){
                                    echo "<option value='{$item['id']}'>{$item['beneficio']}</option>";
                                }
                                ?>
                            </select>
                            <label for="beneficio">Beneficio</label>
                        </div>

                        <div class="input-field col s6">
                            <input name="quantidade" id="quantidade" type="number" required autocomplete="off">
                            <label for="quantidade">quantidade</label>
                        </div>


                        <div class="input-field col s12">
                            <textarea class="materialize-textarea" name="descricao" id="descricao" onkeyup="limite_textarea(this.value,10000)" maxlength="10000" rows="9" required></textarea>
                            <label for="descricao">Descrição</label>
                            <span id="cont">10000</span>/10000
                        </div>

                        <div class="input-field col s12">
                            <input type="submit" class="btn btn-block btn-large green col s12 waves-block waves-effect waves-light" value="Salvar">
                        </div>
                    </form>
                </div>

            </div>
        </div><!--fim do card form-->


            <?php
            try {
            $sql = "SELECT * FROM beneficio WHERE cod_pessoa=? "
                . "ORDER BY data DESC";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->bindParam(1, $_GET['id']);
            $consulta->execute();
            $beneficio = $consulta->fetchAll();
            $cont = $consulta->rowCount();
            $sql = null;
            $consulta = null;
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
            ?>
        <!--card historico-->
        <div class="card light darken-1">
            <div class="card-content blue-grey-text">
                <div class="row">
                    <h4 id="pointofview">Historico de benefícios concedidos</h4>
                    <?php
                    if ($cont!=0){
                        echo "<h6>{$cont} lançamento(s) encontrado(s)</h6><br>";
                        echo "<a href='index.php?pg=Vb_print&id={$_GET['id']}' target='_blank' class='btn tooltipped waves-block waves-effect waves-light' data-position='top' data-tooltip='Abre a tela com o prontuario de benefícios para impressão'>Imprimir histórico</a>";
                    }else{
                        echo "<h5 class='red-text'>Não há historicos dessa pessoa</h5>";
                    }
                    foreach ($beneficio as $ben){
                    ?>
                        <blockquote>
                            Beneficio: <strong><?php echo fncgetbeneficio_lista($ben['cod_beneficio'])['beneficio']?></strong><br>
                            Quantidade: <strong><?php echo $ben['quantidade'];?></strong><br>
                            Data: <strong title="Lançado em <?php echo datahoraBanco2data($ben['data_ts']);?>"><?php echo dataRetiraHora($ben['data']);?></strong><br>
                            Descrição: <strong>"<?php echo $ben['descricao'];?>"</strong><br>
                            <footer>---
                                <?php echo fncgetusuario($ben['cod_usuario'])['nome'];?>
                                (<?php echo fncgetprofissao(fncgetusuario($ben['cod_usuario'])['cod_profissao'])['profissao'];?>)
                            </footer>
                        </blockquote>
                    <?php } ?>
                </div>
            </div>
        </div><!--fim do card historico-->

    </div><!-- fim da coluna central 8 -->

    <button type="button" onclick="Mudarestado('dados')" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Mostrar dados dessa pessoa">Mostrar dados</button>
        <?php
        include_once("inclusoes/menu_lateral.php")
        ?>

    </div>
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>