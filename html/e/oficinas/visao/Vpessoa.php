<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page=" Pessoa ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $pessoa=fncgetpessoa($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col s8">
            <div class="card light darken-1">
                <?php
                include_once("inclusoes/cabecalho.php");
                if (!$pessoa['status']){
                    echo "<div class='card-action'>";
                    echo "<a href='index.php?pg=Vpessoa&id={$_GET['id']}&aca=pessoaativar' class='btn blue-grey btn-block'>Habilitar para oficinas</a>";
                    echo "</div>";
                }
                ?>
            </div>
        </div>

        <?php
        include_once("inclusoes/menu_lateral.php")
        ?>

    </div>
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>