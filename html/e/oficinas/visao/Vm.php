<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Crianças com cadastro ativo ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

try{
    $sql="SELECT * FROM ";
    $sql.="pessoa ";
    $sql.="WHERE status=1 order by nome ";
    global $pdo;
    $consulta=$pdo->prepare($sql);
    $consulta->execute();
    $arrr = $consulta->fetchAll();
    $cont=$consulta->rowCount();
    $sql=null;
    $consulta=null;
}catch ( PDOException $error_msg){
    echo 'Erroff'. $error_msg->getMessage();
}
?>
    <main class="container">
        <h2>Lista de crianças com cadastro ativo</h2>
        <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <td>Nome</td>
                <td>Nascimento</td>
                <td>Nome da Mae</td>
                <td>Endereço</td>
                <td>Bairro</td>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($arrr as $pessoa){
                $id=$pessoa['id'];
                $nome=$pessoa['nome'];
                if ($pessoa["nascimento"]=="1000-01-01"){
                    $nascimento = "--/--/----";
                }else{
                    $nascimento = dataBanco2data($pessoa["nascimento"]);
                }
                $mae=$pessoa['mae'];
                $endereco=$pessoa['endereco'];
                if ($pessoa['numero']==0){$numero="S/N";}else{$numero=$pessoa['numero'];}
                $bairro=fncgetbairro($pessoa['cod_bairro'])['bairro'];

                ?>
                <tr>
                    <td><?php echo $nome;?></td>
                    <td><?php echo $nascimento;?></td>
                    <td><?php echo $mae;?></td>
                    <td><?php echo $endereco;?> <?php echo $numero;?></td>
                    <td><?php echo $bairro;?></td>
                </tr>
            <?php } ?>
            </tbody>
    </table>
    </main>

<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>