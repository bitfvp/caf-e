<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Crianças com cadastro ativo ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>
    <main class="container">
        <div class="row">
            <div class="col s8 offset-s2">
                <h3 class="red-text">Desativar Cadastros para Inicio de temporada</h3>
                <h5>Na data estipulada pela cordenação, todos os cadastros de crianças e adolescentes será desativado e aguardaram atualização e re-matricula para voltar a frequentar as oficinas.  </h5>
                <h6 class="blue-text">Deseja Desativar os cadastros agora?</h6>
                <a class="waves-effect waves-light btn btn-large green" href="index.php">Cancelar</a>

                <script>
                    $(document).ready(function(){
                        $("#dropdown-trigger").dropdown();
                    });
                </script>
                <a id="dropdown-trigger" class='dropdown-trigger waves-effect waves-light btn btn-large red' href='#' data-target='dropdown1'>Desativar cadastros</a>
                <ul id='dropdown1' class='dropdown-content'>
                    <li class="red-text"><a href="index.php?aca=cadastrosdesativar" class="red-text">Apagar Realmente??</a></li>
                </ul>
            </div>
        </div>
    </main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>