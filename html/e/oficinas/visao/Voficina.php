<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page=" Oficina ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $oficina=fncgetoficina($_GET['id']);
}else{
    $_SESSION['fsh']=[
        "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
        "type"=>"danger",
    ];
    header("Location: index.php");
    exit();
}
?>
<main class="container">
    <div class="row">

        <div class="col s5">
            <div class="card light darken-1">
                <div class="card-content blue-grey-text">
                    <span class="card-title"><?php echo $oficina['oficina'];?></span>
                    Oficineiro:<strong><?php echo $oficina['oficineiro'];?></strong><br>
                    Cadastrada em:<strong><?php echo dataRetiraHora($oficina['data_cadastro']);?></strong><br>
                    Cadastrada por:<strong><?php echo fncgetusuario($oficina['cod_usuario'])['nome'];?></strong><br><br>
                    Status:<strong>
                        <?php
                        if ($oficina['status']==1){
                            echo "<strong class='green-text'>Oficina Ativa</strong>";
                        }else{
                            echo "<strong class='red-text'>Oficina Desativada</strong>";
                        }
                        ?>
                    </strong>
                    <br>
                    <a href='index.php?pg=Voficina_print&id=<?php echo $_GET['id'];?>' target='_blank'>Imprimir oficina</a>
                </div>
                <div class="card-action">
                    <a href="index.php?pg=Voficina_editar&id=<?php echo $oficina['id']?>"  class="btn blue btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Alterar dados da oficina">Editar</a>
                </div>
            </div>
        </div>

        <?php
        $sql = "SELECT * FROM oficina_inscrito WHERE cod_oficina=?";
        global $pdo;
        $consulta = $pdo->prepare($sql);
        $consulta->bindParam(1, $oficina['id']);
        $consulta->execute();
        $inscritos = $consulta->fetchall();
        $inscritos_count = $consulta->rowCount();
        $sql=null;
        $consulta=null;
        ?>


        <script type="text/javascript">
            $(document).ready(function () {
                $('#serch1 input[type="text"]').on("keyup input", function () {
                    /* Get input value on change */
                    var inputVal = $(this).val();
                    var resultDropdown = $(this).siblings(".result");
                    if (inputVal.length) {
                        $.get("<?php echo $env->env_url_mod;?>inclusoes/oficina_add_inscrito.php", {term: inputVal,cod_oficina: "<?php echo $oficina['id'];?>", pg: "<?php echo $_GET['pg'];?>"} ).done(function (data) {
                            // Display the returned data in browser
                            resultDropdown.html(data);
                        });
                    } else {
                        resultDropdown.empty();
                    }
                });

                // Set search input value on click of result item
                $(document).on("click", ".result p", function () {
                    $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
                    $(this).parent(".result").empty();
                });
            });
        </script>

        <div class="col s7" id="serch1">
            <?php
            if ($oficina['status']==1){
            echo "<input type='text' class='small' id='admembro' placeholder='Adicionar Criança na Oficina'>";
            }
            ?>
            <div class="result"></div>
            <h6>
                <?php echo $inscritos_count." inscrito(s) nessa oficina";?>
            </h6>
            <ul class="collection">
                <?php
                foreach ($inscritos as $item){
                    //
                    echo "<li class='collection-item'>";
                    echo "<a href='index.php?pg=Vpessoa&id=".fncgetpessoa($item['cod_pessoa'])['id']."'>".fncgetpessoa($item['cod_pessoa'])['nome']."</a>";
                    if ($oficina['status']==1) {
                        echo "<a class='btn-small red right accent-5 tooltipped waves-effect waves-light' data-position='top' data-tooltip='Remover pessoa da oficina' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=excloficina&membro={$item['id']}'>";
                        echo "<i class='material-icons'>delete_forever</i>";
                        echo "</a>";
                    }
                    echo "</li>";
                }
                ?>
            </ul>
        </div>

    </div>
</main>


<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>