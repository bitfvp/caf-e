<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    //validação das permissoes
    //if ()
}
$page="Oficina editar ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="oficinasave";
    $oficina=fncgetoficina($_GET['id']);
}else{
    $a="oficinanew";
}
?>
<main class="container">
    <div class="row">
        <h4>Oficina</h4>
        <hr>
    </div>

    <div class="row">
        <form action="<?php echo "index.php?pg=Voficina_editar&aca={$a}"; ?>" method="post" class="col s12">

            <div class="row">
                <div class="col s6">
                    <br>
                    <input type="submit" value="Salvar"  class="btn btn-block green col s12  waves-block waves-effect waves-light">
                </div>

                <div class="col s6">
                    <br>
                    <a href="index.php?pg=Voficina_editar" class="btn btn-block tooltipped waves-block waves-effect waves-light" data-position="top" data-tooltip="Abrir novo cadastro em branco">Nova Oficina</a>
                </div>
            </div>

            <div class="row">
                <h4>Dados</h4>
                <div class="input-field col s12">
                    <input name="id"  id="id" type="hidden" value="<?php if (isset($oficina['id'])){echo $oficina['id'];} ?>"/>
                    <input name="oficina" id="oficina" type="text" class="validate" value="<?php if (isset($oficina['oficina'])){echo $oficina['oficina'];} ?>">
                    <label for="nome">Oficina</label>
                </div>
                <div class="input-field col s12">
                    <input name="oficineiro" id="oficineiro" type="text" class="validate" value="<?php if (isset($oficina['oficineiro'])){echo $oficina['oficineiro'];} ?>">
                    <label for="oficineiro">Oficineiro</label>
                </div>
                <div class="input-field col s3">
                    <select name="status" required="true">
                        <option selected="" value="<?php if (isset($oficina['status'])){echo $oficina['status'];} ?>">
                            <?php
                            if (isset($oficina['status'])){
                                if ($oficina['status']==1){
                                    echo "ATIVO";
                                }else{
                                    echo "DESATIVADO";
                                }
                            }
                            ?>
                        </option>
                        <option value="1">ATIVO</option>
                        <option value="0">DESATIVADO</option>
                    </select>
                    <label>Status</label>
                </div>
            </div>

        </form>
    </div>

</main>

<?php include_once("{$env->env_root}inclusoes/footer.php"); ?>
