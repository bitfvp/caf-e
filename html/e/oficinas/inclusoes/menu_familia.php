
<script type="text/javascript">
    $(document).ready(function () {
        $('#serch1 input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings(".result");
            if (inputVal.length) {
                $.get("<?php echo $env->env_url_mod;?>inclusoes/pessoa_add_cf.php", {term: inputVal,cf: "<?php echo $pessoa['cod_familiar'];?>", id: "<?php echo $pessoa['id'];?>", pg: "<?php echo $_GET['pg'];?>"} ).done(function (data) {
                    // Display the returned data in browser
                    resultDropdown.html(data);
                });
            } else {
                resultDropdown.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".result p", function () {
            $(this).parents("#serch1").find('input[type="text"]').val($(this).text());
            $(this).parent(".result").empty();
        });
    });
</script>

<div class="col s4" id="serch1">
    <a href="index.php?pg=Vpessoa&id=<?php echo $_GET['id'];?>">Família</a>

    <?php
    if(isset($pessoa['cod_familiar']) and $pessoa['cod_familiar']!=null and $pessoa['cod_familiar']!=0) {
    //existe um id e se ele é numérico
    $sql = "SELECT * FROM pessoa WHERE cod_familiar=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $pessoa['cod_familiar']);
    $consulta->execute();
    $cfpessoa = $consulta->fetchall();
    $sql=null;
    $consulta=null;
    ?>

    <ul class="collection">
        <?php
            foreach ($cfpessoa as $cfp){
                //
                echo "<li class='collection-item'>";
                echo "<a href='index.php?pg=Vpessoa&id={$cfp['id']}'>{$cfp['nome']}</a>";
                echo "<a class='btn-small red right accent-5 tooltipped waves-effect waves-light' data-position='top' data-tooltip='Remover pessoa da família' href='index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=exclmembro&idmembro={$cfp['id']}'>";
                echo "<i class='material-icons'>delete_forever</i>";
                echo "</a>";
                echo "</li>";
            }
            ?>
    </ul>
    <input type="text" class="small" id="admembro" placeholder="Adicionar membro da família">
        <div class="result"></div>
        <?php
    }else{
        echo "<a href=\"index.php?pg={$_GET['pg']}&id={$_GET['id']}&aca=novocf\" class=\"btn btn-warning btn-block\">Gerar Família</a>";
    }
    ?>
</div>

<!--<script type="application/javascript">-->
<!--    var offset = $('#sidebarf').offset().top;-->
<!--    var $meuMenuf = $('#sidebarf'); // guardar o elemento na memoria para melhorar performance-->
<!--    $(document).on('scroll', function () {-->
<!--        if (offset <= $(window).scrollTop()) {-->
<!--            $meuMenuf.addClass('fixarmenuf');-->
<!--        } else {-->
<!--            $meuMenuf.removeClass('fixarmenuf');-->
<!--        }-->
<!--    });-->
<!--</script>-->