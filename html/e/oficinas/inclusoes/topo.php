<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
        <style>
            .logologo{
                height: 60px;
                padding: 0;
                padding-top: 3px;
                margin: 0;
            }
        </style>
        <a id="logo-container" href="<?php echo $env->env_url_mod;?>" class="brand-logo">
            <img class="logologo left" src="<?php echo $env->env_estatico;?>img/caf-e.png" alt="">
        </a>

        <ul id="nav_matricula" class="dropdown-content">
            <li><a href="index.php?pg=Vm">Crianças com Cadastro Ativo</a></li>
            <li><a href="index.php?pg=Vm_reset">Desativar Cadastros para Inicio de temporada</a></li>
        </ul>
        <ul id="nav_nav_matricula" class="dropdown-content">
            <li><a href="index.php?pg=Vm">Crianças com Cadastro Ativo</a></li>
            <li><a href="index.php?pg=Vm_reset">Desativar Cadastros para Inicio de temporada</a></li>
        </ul>

        <ul id="nav_oficina" class="dropdown-content">
            <li><a href="index.php?pg=Voficina_lista">Listas Oficinas</a></li>
        </ul>
        <ul id="mob_nav_oficina" class="dropdown-content">
            <li><a href="index.php?pg=Voficina_lista">Listas Oficinas</a></li>
        </ul>

        <ul id="nav_user" class="dropdown-content">
            <li><a class="indigo-text" href="index.php?pg=Vtrocasenha">Alterar senha</a></li>
            <li><a class="indigo-text" href="<?PHP echo $env->env_url; ?>?pg=Vlogin">Voltar</a></li>
            <li><a class="indigo-text" href="?aca=logout">Sair</a></li>
        </ul>
        <ul id="mob_nav_user" class="dropdown-content">
            <li><a class="indigo-text" href="index.php?pg=Vtrocasenha">Alterar senha</a></li>
            <li><a class="indigo-text" href="<?PHP echo $env->env_url; ?>?pg=Vlogin">Voltar</a></li>
            <li><a class="indigo-text" href="?aca=logout">Sair</a></li>
        </ul>

        <ul class="right hide-on-med-and-down">
            <li><a href="index.php?pg=Vpessoa_lista"><i class="material-icons">search</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="nav_matricula">Sobre Matriculas<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="nav_oficina">Oficinas<i class="material-icons right">arrow_drop_down</i></a></li>
            <li>
                <a class="dropdown-trigger" href="#!" data-target="nav_user"><i class=' material-icons'>account_circle</i></a>
            </li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="index.php?pg=Vpessoa_lista"><i class="material-icons">search</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="mob_nav_matricula">Sobre Matriculas<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-trigger" href="#!" data-target="mob_nav_oficina">Oficinas<i class="material-icons right">arrow_drop_down</i></a></li>
            <li>
                <a class="dropdown-trigger" href="#!" data-target="mob_nav_user"><i class=' material-icons'>account_circle</i></a>
            </li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>