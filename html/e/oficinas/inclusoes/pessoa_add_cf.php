<?php
////ativa a exibição de erros do php/pdo
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/caf-e/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/caf-e/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();

define('HOST',$_ENV['ENV_BD_IP']);
define('USER',$_ENV['ENV_BD_USUARIO']);
define('PASS',$_ENV['ENV_BD_SENHA']);
define('DB',$_ENV['ENV_BD_BANCO']);

$conexao = 'mysql:host=' . HOST . ';dbname=' . DB;
try {
    global $pdo;
    $pdo = new PDO($conexao, USER, PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES utf8');
} catch (PDOException $error_pdo) {
    echo 'erro ao conectar a base de dados' . $error_pdo->getMessage();
}
////////////////////////////////////////////////////////////////////////////////////////////

$sc=$_GET['term'];
$cf=$_GET['cf'];
$id=$_GET['id'];
$pg=$_GET['pg'];

//echo "<pre>";
//print_r($_GET);
//echo "</pre>";

    $sql = "SELECT * FROM pessoa WHERE nome LIKE '%$sc%' limit 0,10";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    $lipessoa = $consulta->fetchall();
    $sql=null;
    $consulta=null;

    $sta = strtoupper($sc);
    define('CSA', $sta);

    foreach ($lipessoa as $lp){
        echo "<div class='row'>";
            $sta = CSA;
            $nnn = strtoupper($lp["nome"]);
            $nn = explode(CSA, $nnn);
            $n = implode("<span class='red-text'>{$sta}</span>", $nn);
            echo $n;

        echo "<a href='index.php?pg={$pg}&id={$id}&aca=addmembro&cf={$cf}&idmembro={$lp["id"]}' class='btn green right waves-effect waves-light'><i class='material-icons'>person_add</i></a>";
        echo "</div>";
    }