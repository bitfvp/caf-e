<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
        //validação das permissoes
        //if ()
}
$page="Lançador ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
?>

<div class="container">
    <style>
        /*apresentaÃ§Ã£o dos modulos*/
        .cta {
            position: relative;
            width: auto;
            padding: 5px 5px;
            border: 1px solid black;
            margin-top: 10px;
            margin-bottom: 10px;
            border-radius: 7px;
        }
        .cta > .row {
            display: table;
            width: calc(100% - 0px);
        }
        .cta > .row > [class^="col-"],
        .cta > .row > [class*=" col-"] {
            float: none;
            display: table-cell;
            vertical-align: middle;
        }

        .cta-title {
            margin: 0 auto 0px;
            padding: 0;
        }
        .cta-desc {
            padding: 0;
        }
        .cta-desc p:last-child {
            margin-bottom: 0;
        }
         #bb {
             float: none;
             display: table-cell;
             vertical-align: middle;
        }
    </style>

    <div class="cta light-blue">
        <div class="row">
            <div class="col s9">
                <h2 class="cta-title">Famílias</h2>
                <div class="cta-desc">
                    <p>Módulo de controle de atividades, benefícios e atenção básica.</p>
                </div>
            </div>
            <div id="bb" class="col s3 ">
                <a href='familias' class="btn btn-large btn-block green cta-button waves-effect waves-light waves-block">Acessar!</a>
            </div>
        </div>
    </div>

    <div class="cta light-blue">
        <div class="row">
            <div class="col s9">
                <h2 class="cta-title">Oficinas</h2>
                <div class="cta-desc">
                    <p>Módulo de gestão de oficinas e inscricões.</p>
                </div>
            </div>
            <div id="bb" class="col s3 ">
                <a href='oficinas' class="btn btn-large btn-block green cta-button waves-effect waves-light waves-block">Acessar!</a>
            </div>
        </div>
    </div>

    <div class="cta light-blue">
        <div class="row">
            <div class="col s9">
                <h2 class="cta-title">Contatos</h2>
                <div class="cta-desc">
                    <p>Agenda telefônica destinada a contatos com oficineiros e voluntarios.</p>
                </div>
            </div>
            <div id="bb" class="col s3 ">
                <a href='agenda' class="btn btn-large btn-block green cta-button waves-effect waves-light waves-block">Acessar!</a>
            </div>
        </div>
    </div>

    <div class="cta light-blue">
        <div class="row">
            <div class="col s9">
                <h2 class="cta-title">Usúarios</h2>
                <div class="cta-desc">
                    <p>Adminstração de contas de usúario.</p>
                </div>
            </div>
            <div id="bb" class="col s3 ">
                <a href='usuarios' class="btn btn-large btn-block yellow black-text cta-button waves-effect waves-red waves-block">Acessar!</a>
            </div>
        </div>
    </div>

</div>
<br>
<br>
<?php
include_once("{$env->env_root}inclusoes/footer.php");
?>
