<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    //validação das permissoes
    //if ()
}
$page="Usuario editar ".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}inclusoes/head.php");
include_once("inclusoes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="usuariosave";
    $usuario=fncgetusuario($_GET['id']);
}else{
    $a="usuarionew";
}
?>
<main class="container">
    <div class="row">
        <h4>Usuário</h4>
        <hr>
    </div>

    <div class="row">
        <form action="<?php echo "index.php?pg=Vhome&aca={$a}"; ?>" method="post" class="col s12">
<!--            -->
            <div class="row">
                <div class="col s6">
                    <br>
                    <input type="submit" value="Salvar" class="btn btn-block green col s12 waves-block waves-effect waves-light">
                </div>

                <div class="col s6">
                    <br>
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){?>
                        <a href="index.php?pg=Vusuario_editar&id=<?php echo $_GET['id'];?>&aca=resetsenha" class="btn btn-block waves-block waves-effect waves-light">Resetar senha</a>
                    <?php } ?>
                </div>
            </div>
<!--            -->
            <div class="row">
                <div class="input-field col s6">
                    <input name="id"  id="id" type="hidden" value="<?php if (isset($usuario['id'])){echo $usuario['id'];} ?>"/>
                    <input name="nome" id="nome" type="text" class="validate" value="<?php if (isset($usuario['nome'])){echo $usuario['nome'];} ?>">
                    <label for="nome">Nome</label>
                </div>

                <div class="input-field col s6">
                    <input name="email" id="email" type="email" class="validate" value="<?php if (isset($usuario['email'])){echo $usuario['email'];} ?>">
                    <label for="email">E-mail</label>
                </div>


            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input name="nascimento" id="nascimento" type="date" value="<?php echo $usuario['nascimento']; ?>">
                    <label for="nascimento">Nascimento</label>
                </div>

                <div class="input-field col s6">
                    <select name="cod_profissao" required="true">
                        <option selected="" value="<?php if (isset($usuario['cod_profissao'])){echo $usuario['cod_profissao'];} ?>">
                            <?php
                            if (isset($usuario['cod_profissao'])){
                            $profissaoid=$usuario['cod_profissao'];
                            $profissao=fncgetprofissao($profissaoid);
                            echo $profissao['profissao'];
                            }
                            ?>
                        </option>
                        <?php
                        foreach(fncprofissaolist() as $item){?>
                            <option value="<?php echo $item['id']; ?>"><?php echo $item['profissao']; ?></option>
                        <?php } ?>
                    </select>
                    <label>Profissão</label>
                </div>

                <div class="input-field col s3">
                    <select name="status" required="true">
                        <option selected="" value="<?php if (isset($usuario['status'])){echo $usuario['status'];} ?>">
                            <?php
                            if (isset($usuario['status'])){
                                if ($usuario['status']==1){
                                    echo "ATIVO";
                                }else{
                                    echo "DESATIVADO";
                                }
                            }
                            ?>
                        </option>
                        <option value="1">ATIVO</option>
                        <option value="0">DESATIVADO</option>
                    </select>
                    <label>Status</label>
                </div>
            </div>

        </form>
    </div>

</main>

<?php include_once("{$env->env_root}inclusoes/footer.php"); ?>
