<?php
//inicia cookies
ob_start();
//inicia sessions
session_start();

//reports
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);


//api que verifica informacoes referente ao ambiente de utilização da aplicacao
$pathFile = realpath($_SERVER['DOCUMENT_ROOT']."/caf-e/vendor/autoload.php") ;
if ($pathFile) {
    $realroot=$_SERVER['DOCUMENT_ROOT']."/caf-e/";
} else {
    $realroot="/var/www/";
}

require $realroot."vendor/autoload.php";
$dotenv = new Dotenv\Dotenv( $realroot );
$dotenv->load();
/*
*   inclui e inicia a classe de configuração de ambiente que é herdada de uma classe abaixo da raiz em models/Env.class.php
*   $env e seus atributos sera usada por todo o sistema
*/
include_once("modelos/ConfigMod.class.php");
$env=new ConfigMod();

/*
*   inclui e inicia a classe de configuração de drive pdo/mysql
*   $pdo e seus atributos sera usada por todas as querys
*/
include_once("{$env->env_root}modelos/Db.class.php");////conecção com o db
$pdo = Db::conn();


//inclui um controle contendo a funcão que apaga a sessao e desloga
//será chamada da seguinte maneira
//killSession();
include_once("{$env->env_root}controles/Ks.php");//se ha uma acao

//inclui um controle que ativa uma acao
include_once("{$env->env_root}controles/action.php");//se ha uma acao

//funcoes diversas
include_once ("{$env->env_root}inclusoes/funcoes.php");//funcoes

////login
/// classe para login
include_once("{$env->env_root}modelos/Login.class.php");
///controle para login
include_once("{$env->env_root}controles/login.php");

//logout
//controle pra logout
include_once("{$env->env_root}controles/logout.php");

//validação de sessoes ativas
//inclui um controle que valida o hash e renova o mesmo se tiver tudo certo
include_once("{$env->env_root}controles/validaToken.php");


//metodo de checar usuario, confirma se a session que vincula o login ao hash de sessao no banco de dados esta ativa
include_once("{$env->env_root}controles/confirmUser.php");