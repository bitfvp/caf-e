<?php
echo '<link href="'.$env->env_estatico.'css/reset.css" rel="stylesheet" />'."\n";
echo '<script src="' .$env->env_estatico . 'js/jquery.min.js"></script>' . "\n";
echo "<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.13/css/all.css' integrity='sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp' crossorigin='anonymous'>";
if ($css=="style1"){
    echo '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />'."\n";
    echo '<link href="'.$env->env_estatico.'css/materialize.css" rel="stylesheet" />'."\n";
    echo '<link href="'.$env->env_estatico.'css/style.css" rel="stylesheet" />'."\n";

    echo '<script src="' .$env->env_estatico . 'js/materialize.js"></script>' . "\n";
    echo '<script src="' .$env->env_estatico . 'js/init.js"></script>' . "\n";
}
if ($css=="portfolio"){
    echo '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />'."\n";
    echo '<link href="' .$env->env_estatico . 'css/materialize.css" rel="stylesheet" />'."\n";
    echo '<link href="'.$env->env_estatico.'css/style.css" rel="stylesheet" />'."\n";

    echo '<script src="' .$env->env_estatico . 'js/materialize.js"></script>' . "\n";

    echo '<link href="' .$env->env_estatico . 'css/portifolio.css" type="text/css" rel="stylesheet" media="screen,projection"/>'."\n";
    echo '<script src="' .$env->env_estatico . 'js/portifolio.js"></script>' . "\n";
    echo '<script src="' .$env->env_estatico . 'js/init.js"></script>' . "\n";
}
if (!isset($css) or $css=="404"){
    echo'<link href="'.$env->env_estatico.'css/404.css" rel="stylesheet" media="all" />'."\n";
    echo '<script src="' .$env->env_estatico . 'js/init.js"></script>' . "\n";
}
//caso nao tenha css setado
if (!isset($css) or $css==""){
    //Materialize Core CSS
  //  echo'<link href="'.$env->envHomeSurce.'css/Materialize.min.css" rel="stylesheet" />'."\n";
}
echo "\n";