<?php
$page="Login-".$env->env_titulo;
$css="style1";
if(isset($_SESSION['logado'])){
    header("Location: {$env->env_url}e");
    exit();
}
include_once("{$env->env_root}inclusoes/head.php");
?>
    <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #fff;
        }

        .input-field input[type=date]:focus + label,
        .input-field input[type=text]:focus + label,
        .input-field input[type=email]:focus + label,
        .input-field input[type=password]:focus + label {
            color: #e91e63;
        }

        .input-field input[type=date]:focus,
        .input-field input[type=text]:focus,
        .input-field input[type=email]:focus,
        .input-field input[type=password]:focus {
            border-bottom: 2px solid #e91e63;
            box-shadow: none;
        }
    </style>
    <div class="section"></div>
    <main>
        <center>
            <a href="index.php">
            <img class="responsive-img" style="width: 250px;" src="<?php echo $env->env_estatico;?>img/caf.png" />
            </a>
            <div class="section"></div>

            <h5 class="indigo-text">Login</h5>
            <div class="section"></div>

            <div class="container">
                <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

                    <form class="col s12" action="index.php?pg=Vlogin&aca=logar" method="post" >

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input class='validate' autofocus type='email' name='email' id='email' />
                                <label for='email'>SeuEmail@seuprovedor.com</label>
                            </div>
                        </div>

                        <div class='row'>
                            <div class='input-field col s12'>
                                <input class='validate' type='password' name='password' id='password' />
                                <label for='password'>Sua senha</label>
                            </div>
                        </div>

                        <script type="text/javascript">
                            function selecionaTexto() {
                                document.getElementById("email").select();
                            }
                            window.onload = selecionaTexto();
                        </script>
                        <br />
                        <center>
                            <div class='row'>
                                <button type="submit" class='col s12 btn btn-large waves-effect waves-light indigo'>

                                    Login
                                </button>
                            </div>
                        </center>
                    </form>
                </div>
            </div>
        </center>

        <div class="section"></div>
        <div class="section"></div>
    </main>

<?php
include_once("{$env->env_root}inclusoes/footer.php")
?>