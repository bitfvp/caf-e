<?php
$page="CAF - Pagina inicial ".$env->env_titulo;
$css="portfolio";
include_once("{$env->env_root}inclusoes/head.php");
?>
<div class="navbar-fixed">
    <nav class="grey darken-4">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">CAF - MANHUAÇU</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="#atividades">Atividades</a></li>
                <li><a href="#contato">Contato</a></li>
                <li><a href="index.php?pg=Vlogin">Administrativo</a></li>
            </ul>
            <ul id="nav-mobile" class="sidenav sob">
                <li><a href="index.php">Inicio</a></li>
                <li><a href="#atividades">Atividades</a></li>
                <li><a href="#contato">Contato</a></li>
                <li><a href="index.php?pg=Vlogin">Administrativo</a></li>
            </ul>
            <style>
                .sidenav-overlay{
                   z-index: -1;
               }
            </style>
            <a href="!#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</div>
<div class="had-container">
    <div class="parallax-container prof-user">
        <div class="parallax"><img src="<?php echo $env->env_estatico;?>img/cafcr.JPG"></div>
        <div class="row"><br>
            <div class="col m8 s8 offset-m2 offset-s2 center">
                <h4 class="truncate bg-card-user">
                    <img src="<?php echo $env->env_estatico;?>img/caf.png" alt="" class=" responsive-img"> <br>
                    <strong class="black-text"><strong class="red-text">C</strong>ENTRO DE <strong class="red-text">A</strong>POIO À <strong class="red-text">F</strong>AMÍLIA</strong>
                </h4>
            </div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col xl12 l12 m12 s12">
        <div class="card-panel center teal">
                <span class="white-text">
                    O CAF é uma entidade filantrópica sem fins lucrativos, desenvolve ações de profissionalização e inserção no mercado de trabalho, orientação e apoio sócio-familiar para crianças, adolescentes e seus familiares.A entidade foi fundada pela Casa Espírita Boa Nova,atualmente Casa Espírita Manoel Henrique.
                </span>
        </div>
    </div> <!-- fin del .lms12 -->

    <!-- Grid de promocao -->
    <div class="col xl4 l4 m12 12" id="atividades">
        <div class="card-panel center promo blue-grey darken-3 white-text">
            <i class="material-icons">insert_emoticon</i>
            <p class="promo-caption">Oficinas</p>
            <p class="light center">Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
        </div>
    </div>
    <div class="col xl4 l4 m12 12">
        <div class="card-panel center promo blue-grey darken-3 white-text">
            <i class="material-icons">group</i>
            <p class="promo-caption">Trabalhos em grupo</p>
            <p class="light center">Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
        </div>
    </div>
    <div class="col xl4 l4 m12 12">
        <div class="card-panel center promo blue-grey darken-3 white-text">
            <i class="material-icons">accessibility</i>
            <p class="promo-caption">Atendimento</p>
            <p class="light center">Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
        </div>
    </div>
    <!-- Grid de promocao -->
</div> <!-- fim do .row -->

<div class="parallax-container">
    <div class="parallax"><img src="<?php echo $env->env_estatico;?>img/caf2.JPG"></div>
</div>

<div class="row">
    <!-- img --> <br>
    <div class="col xl3 l3 m3 s12">
        <img class="materialboxed" data-caption="Fotografía" width="100%" src="<?php echo $env->env_estatico;?>img/criancas1.jpg">
    </div>
    <div class="col xl3 l3 m3 s12">
        <img class="materialboxed" data-caption="Fotografía" width="100%" src="<?php echo $env->env_estatico;?>img/criancas2.jpg">
    </div>
    <div class="col xl3 l3 m3 s12">
        <img class="materialboxed" data-caption="Fotografía" width="100%" src="<?php echo $env->env_estatico;?>img/criancas3.jpg">
    </div>
    <div class="col xl3 l3 m3 s12">
        <img class="materialboxed" data-caption="Fotografía" width="100%" src="<?php echo $env->env_estatico;?>img/criancas4.jpg">
    </div>
    <!-- imgs -->
</div> <!-- fim de .row -->

<div class="row light-blue">
    <!-- doar --> <br>
    <div class="col xl6 l6 m6 s12">
        <h3>Seja Voluntário</h3>
        <h5>
            Precisamos da colaboração de todos, cidadãos e corporações, para manter e fortalecer cada vez mais o trabalho do CAF.
        </h5>
        <h6>
            <strong>Para ser voluntário (a) no CAF, é necessário:</strong>
        <ul>
            <li><i class="fa fa-star"></i> Ser maior de 18 anos;</li>
            <li><i class="fa fa-star"></i> Ter disposição, comprometimento e espírito de equipe;</li>
            <li><i class="fa fa-star"></i> Ter sido entrevistado e aprovado pela Administração Entidade e/ou Direção e encaminhado à atividade mais adequada ao perfil, quando houver.</li>
        </ul>
        </h6>

    </div>

    <div class="col xl6 l6 m6 s12">
        <h3>O que doar</h3>
        <h5>Doar é um ato de cidadania e solidariedade capaz de elevar a felicidade do doador, gerando bem-estar psicológico e emocional. </h5>
        <h6>
            <strong class="left">Aceitamos:</strong><br>
            <ul>
                <li><i class="fa fa-star"></i> Materiais de escritório;</li>
                <li><i class="fa fa-star"></i> Móveis novos ou em bom estado, de revenda, ponta de estoque ou fora de linha</li>
                <li><i class="fa fa-star"></i> Roupas novas e seminovas</li>
                <li><i class="fa fa-star"></i> Brinquedos novos ou em bom estado;</li>
                <li><i class="fa fa-star"></i> Material esportivo e de recreação;</li>
                <li><i class="fa fa-star"></i> Equipamentos de informática novos ou em bom estado;</li>
                <li><i class="fa fa-star"></i> Eletrodomésticos e eletroeletrônicos novos ou em bom estado.</li>
            </ul>
        </h6>
    </div>
    <!-- doar -->
</div> <!-- fim de .row -->


<!--contato-->
<div class="col s12 m6 l4 " id="contato">
    <div class="containe center">
        <br/>
        <h4 id="side_bar" class="blue-text">Entre em contato conosco:</h4>
        <div class="row">
            <div class="col s12 center">
                <b style="font-size: 18px;">Endereço</b><br/>
                <p style="font-size: 15px;">Av. Hervé Cordovil, São Francisco de Assis,
                    <br/>Manhuaçu - MG,
                    <br/>Brasil.
                </p>
                <hr>

                <b style="font-size: 18px;">Telefone</b><br/>
                <i class="material-icons md-59">phone</i> (33) ***********
                <hr>

                <b style="font-size: 18px;">Email</b><br/>
                <i class="material-icons">email</i> email@****.com
            </div>
        </div>
    </div>
</div>
<!--contato-->

<footer class="page-footer grey darken-4">
    <div class="footer-copyright">
        <div class="container center">
            © 2018 Copyright Berranteiro sistemas
        </div>
    </div>
</footer>

</body>
</html>