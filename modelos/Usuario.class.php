<?php 
class Usuario{
    public function fncusuarionew($status,$nome,$email,$senha,$nascimento,$cod_profissao){

        //valida se ja ha um usuario cadastrado
        try{
            $sql="SELECT * FROM usuario WHERE email=:email";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":email", $email);
            $consulta->execute();

        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        $contar=$consulta->rowCount();
        if($contar==0){
            //inserção no banco
            try{
                $sql="INSERT INTO usuario(id, status,data_criacao,nome,email,senha,nascimento,cod_profissao)"
                                ." VALUES(NULL, :status, CURRENT_TIMESTAMP, :nome,:email,:senha,:nascimento,:cod_profissao)";

                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":status", $status);
                $insere->bindValue(":nome", $nome);
                $insere->bindValue(":email", $email);
                $insere->bindValue(":senha", $senha);
                $insere->bindValue(":nascimento",$nascimento);
                $insere->bindValue(":cod_profissao",$cod_profissao);
                $insere->execute();

            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já há um usuario cadastrado com esse email em nosso sistema!!!!",
                "type"=>"danger",
            ];

        }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: ?pg=Vhome");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }











    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncusuarioedit($id,$status,$nome,$email,$nascimento,$cod_profissao){

        //inserção no banco
        try{
            $sql="UPDATE usuario SET status=:status, nome=:nome, email=:email, nascimento=:nascimento, cod_profissao=:cod_profissao WHERE id=:id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":status", $status);
            $insere->bindValue(":nome", $nome);
            $insere->bindValue(":email", $email);
            $insere->bindValue(":nascimento", $nascimento);
            $insere->bindValue(":cod_profissao", $cod_profissao);
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vhome");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }



    //////////////////////////////////////////
    public function fncnewsenha($id,$senha){
        //tratamento das variaveis
        $senha=sha1($senha."cachorro");

        //atualização no banco
        try{
            $sql="UPDATE usuario SET senha=:senha ";
            $sql.="WHERE id=:id";
            global $pdo;
            $at=$pdo->prepare($sql);
            $at->bindValue(":id", $id);
            $at->bindValue(":senha", $senha);
            $at->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }

        if(isset($at)){
            $_SESSION['fsh']=[
                "flash"=>"Atualização de senha realizado com sucesso!!",
                "type"=>"success",
                "error"=>"No proximo login use a nova senha cadastrada",
            ];
            header("Location: index.php");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }

    public function fncresetsenha($id){
        //inserção no banco
        try{
            $sql = "UPDATE usuario SET senha = :novasenha WHERE id = :id";

            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":novasenha", sha1("12345678" . "cachorro"));
            $insere->bindValue(":id", $id);
            $insere->execute();
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Senha resetada com sucesso!! ",
                "type"=>"success",
                "error"=>"<h2>Nova senha: 12345678</h2>"
            ];

            header("Location: index.php");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador ",
                    "type"=>"danger",
                ];

            }
        }
    }

}//fim class
?>