<?php 
class Pessoa{
    public function fncpessoanew($nome, $nome_social, $nascimento, $cod_sexo, $cpf, $rg, $rg_uf, $cn, $ctps, $pai, $mae, $endereco, $numero, $cod_bairro, $referencia, $telefone, $situacao_de_residencia, $alfabetizado, $deficiencia, $deficiencia_desc){
        $status=0;

        //valida se ja ha um pessoa cadastrado
        try{
            $sql="SELECT * FROM ";
            $sql.="pessoa ";
            $sql.="WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();


        if(($contarcpf==0) or ($cpf=="")){
            //inserção no banco
            try{
                $sql="INSERT INTO pessoa (`id`, `data_cadastro`, `status`, `nome`, `nome_social`, `nascimento`, `cod_sexo`, `cpf`, `rg`, `rg_uf`, `cn`, `ctps`, `pai`, `mae`, `endereco`, `numero`, `cod_bairro`, `referencia`, `telefone`, `situacao_de_residencia`, `alfabetizado`, `deficiencia`, `deficiencia_desc`)"
                    ." VALUES(NULL, CURRENT_TIMESTAMP, :status, :nome, :nome_social, :nascimento, :cod_sexo, :cpf, :rg, :rg_uf, :cn, :ctps, :pai, :mae, :endereco, :numero, :cod_bairro, :referencia, :telefone, :situacao_de_residencia, :alfabetizado, :deficiencia, :deficiencia_desc)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":status",$status);
                $insere->bindValue(":nome",$nome);
                $insere->bindValue(":nome_social",$nome_social);
                $insere->bindValue(":nascimento",$nascimento);
                $insere->bindValue(":cod_sexo",$cod_sexo);
                $insere->bindValue(":cpf",$cpf);
                $insere->bindValue(":rg",$rg);
                $insere->bindValue(":rg_uf",$rg_uf);
                $insere->bindValue(":cn",$cn);
                $insere->bindValue(":ctps",$ctps);
                $insere->bindValue(":pai",$pai);
                $insere->bindValue(":mae",$mae);
                $insere->bindValue(":endereco",$endereco);
                $insere->bindValue(":numero",$numero);
                $insere->bindValue(":cod_bairro",$cod_bairro);
                $insere->bindValue(":referencia",$referencia);
                $insere->bindValue(":telefone",$telefone);
                $insere->bindValue(":situacao_de_residencia",$situacao_de_residencia);
                $insere->bindValue(":alfabetizado",$alfabetizado);
                $insere->bindValue(":deficiencia",$deficiencia);
                $insere->bindValue(":deficiencia_desc",$deficiencia_desc);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse cpf!!",
                "type"=>"warning",
                ];
        }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="pessoa";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;
            $maid=$mid[0];
            header("Location: index.php?pg=Vpessoa&id={$maid}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoaedit($id, $nome, $nome_social, $nascimento, $cod_sexo, $cpf, $rg, $rg_uf, $cn, $ctps, $pai, $mae, $endereco, $numero, $cod_bairro, $referencia, $telefone, $situacao_de_residencia, $alfabetizado, $deficiencia, $deficiencia_desc){
        try{
            $sql="SELECT id FROM ";
            $sql.="pessoa";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //inserção no banco
            try{
                $sql="UPDATE pessoa SET "
                    ."nome=:nome, nome_social=:nome_social, "
                    ."nascimento=:nascimento,cod_sexo=:cod_sexo,cpf=:cpf, "
                    ."rg=:rg,rg_uf=:rg_uf,cn=:cn,ctps=:ctps,pai=:pai,mae=:mae, "
                    ."endereco=:endereco,numero=:numero,cod_bairro=:cod_bairro,referencia=:referencia, "
                    ."telefone=:telefone,situacao_de_residencia=:situacao_de_residencia, "
                    ."alfabetizado=:alfabetizado,deficiencia=:deficiencia,deficiencia_desc=:deficiencia_desc "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":nome",$nome);
                $update->bindValue(":nome_social",$nome_social);
                $update->bindValue(":nascimento",$nascimento);
                $update->bindValue(":cod_sexo",$cod_sexo);
                $update->bindValue(":cpf",$cpf);
                $update->bindValue(":rg",$rg);
                $update->bindValue(":rg_uf",$rg_uf);
                $update->bindValue(":cn",$cn);
                $update->bindValue(":ctps",$ctps);
                $update->bindValue(":pai",$pai);
                $update->bindValue(":mae",$mae);
                $update->bindValue(":endereco",$endereco);
                $update->bindValue(":numero",$numero);
                $update->bindValue(":cod_bairro",$cod_bairro);
                $update->bindValue(":referencia",$referencia);
                $update->bindValue(":telefone",$telefone);
                $update->bindValue(":situacao_de_residencia",$situacao_de_residencia);
                $update->bindValue(":alfabetizado",$alfabetizado);
                $update->bindValue(":deficiencia",$deficiencia);
                $update->bindValue(":deficiencia_desc",$deficiencia_desc);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado  em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoaativar($id){
        try{
            $sql="SELECT id FROM ";
            $sql.="pessoa";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //atualiza no banco
            try{
                $sql="UPDATE pessoa SET "
                    ."status=1 "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vpessoa&id={$id}");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fncpessoadesativar(){
            //atualiza banco
            try{
                $sql="UPDATE pessoa SET "
                    ."status=0 "
                    ."WHERE id>0";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastros reiniciador com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao

}//fim class
?>