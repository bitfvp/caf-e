<?php
class Atividade
{
    public function fncatividadenew($at_pessoa,$at_profissional,$at_data,$at_atividade,$at_descricao)
    {
        //inserção no banco
        try {
            $sql = "INSERT INTO atividade ";
            $sql .= "(id, cod_pessoa, data, data_ts, cod_atividade, descricao, cod_usuario)";
            $sql .= " VALUES ";
            $sql .= "(NULL, :cod_pessoa, :data, CURRENT_TIMESTAMP, :cod_atividade, :descricao, :cod_usuario)";
            global $pdo;
            $insere = $pdo->prepare($sql);
            $insere->bindValue(":cod_pessoa", $at_pessoa);
            $insere->bindValue(":data", $at_data);
            $insere->bindValue(":cod_atividade", $at_atividade);
            $insere->bindValue(":descricao", $at_descricao);
            $insere->bindValue(":cod_usuario", $at_profissional);
            $insere->execute();
        } catch (PDOException $error_msg) {
            echo 'Erro1' . $error_msg->getMessage();
        }
        if (isset($insere)) {

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //arquivo
            $sql = "SELECT Max(id) FROM atividade";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute();
            $mxv = $consulta->fetch();
            $sql = null;
            $consulta = null;
            $maxv = $mxv[0];

            // verifica se foi enviado um arquivo
//            $fillle=$_FILES['arquivo']['name'];
//            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
//                if (is_dir("../../dados/mcu/protecaobasica/atividades/" . $maxv . '/')) {} else {mkdir("../../dados/mcu/protecaobasica/atividades/" . $maxv . '/');}
//                // verifica se foi enviado um arquivo
//                $Upin->get(
//                    '../../dados/mcu/protecaobasica/atividades/'.$maxv.'/', //Pasta de uploads (previamente criada)
//                    $_FILES["arquivo"]["name"], //Pega o nome dos arquivos, altere apenas
//                    10, //Tamanho máximo
//                    "jpg,jpeg,gif,docx,doc,xls,xlsx,pdf,png", //Extensões permitidas
//                    "arquivo", //Atributo name do input file
//                    1 //Mudar o nome? 1 = sim, 0 = não
//                );
//                $Upin->run();
//            }

            // verifica se foi enviado um arquivo
            $fillle=$_FILES['arquivo']['name'];
            if (isset($_FILES['arquivo']['name']) && $fillle[0]!=null) {//if principal
                global $env;
                if (is_dir($env->env_root.'html/arquivos/familias_at/' . $maxv . '/')) {} else {mkdir($env->env_root.'html/arquivos/familias_at/' . $maxv . '/');}

                $storage = new \Upload\Storage\FileSystem($env->env_root.'html/arquivos/familias_at/'. $maxv);
                $file = new \Upload\File('arquivo', $storage);

// Optionally you can rename the file on upload
                $new_filename = uniqid();
                $file->setName($new_filename);

// Validate file upload
// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
                $file->addValidations(array(
                    // Ensure file is of type "image/png"
//                    new \Upload\Validation\Mimetype('image/png'),

                    //You can also add multi mimetype validation
                    new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg', 'image/jpg')),

                    // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                    new \Upload\Validation\Size('5M')
                ));

// Access data about the file that has been uploaded
                $data = array(
                    'name'       => $file->getNameWithExtension(),
                    'extension'  => $file->getExtension(),
                    'mime'       => $file->getMimetype(),
                    'size'       => $file->getSize(),
                    'md5'        => $file->getMd5(),
                    'dimensions' => $file->getDimensions()
                );

// Try to upload file
                try {
                    // Success!
                    $file->upload();
                } catch (\Exception $e) {
                    // Fail!
                    $errors = $file->getErrors();
                }
            }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //obter codigo familiar
            try{
                $sql = "SELECT cod_familiar FROM pessoa WHERE id=?";

                global $pdo;
                $queryy=$pdo->prepare($sql);
                $queryy->bindParam(1, $at_pessoa);
                $queryy->execute();
                $cod_familiar=$queryy->fetch();
            }catch ( PDOException $error_msg){
                echo 'Erro '. $error_msg->getMessage();
            }

            //testar se já tem um codigo familiar valido
            if(isset($cod_familiar['cod_familiar']) and $cod_familiar['cod_familiar']!=null and $cod_familiar['cod_familiar']!=0) {
                $cod_f=$cod_familiar['cod_familiar'];
            }else{
                //se não é valido criar novo codigo familiar
                $token = uniqid("");
                try {
                    $sql = "UPDATE pessoa SET ";
                    $sql .= "cod_familiar=:cod_familiar";
                    $sql .= " WHERE id=:id";

                    global $pdo;
                    $atuali = $pdo->prepare($sql);
                    $atuali->bindValue(":cod_familiar", $token);
                    $atuali->bindValue(":id", $at_pessoa);
                    $atuali->execute();

                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }
                $cod_f=$token;
            }

            $_SESSION['fsh']=[
                "flash"=>"Atividade Cadastrada com sucesso!!",
                "type"=>"success",
                "pointofview"=>"1",
            ];
            header("Location: ?pg=Vat&id={$at_pessoa}");
            exit();

        } else {
            if (empty($fsh)) {
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da fnc new



}//fim da classe

?>