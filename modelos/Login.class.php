<?php
class Login{
    public function fnclogar($email, $senha ){
        //verifica se o banco tem o usuario e senha
	    try{
            $sql="select * from usuario WHERE email=? AND senha=? limit 1";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindParam(1, $email);
            $consulta->bindParam(2, $senha);
            $consulta->execute();
	    }catch ( PDOException $error_msg){
	        echo 'Erroff'. $error_msg->getMessage();
        }
        //verifica se existe >0 registros
        if( $consulta->rowCount()!=0){
			$dados=$consulta->fetch();
			//verifica se esta ativo
			if($dados['status']==1){
                $_SESSION['id']=$dados['id'];
				$_SESSION['nome']=$dados['nome'];
                $_SESSION['logado']="1";
				$log=1;

				//seta o token
                $tml = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
                $tm2=$tml;
                $newtm = $tm2+"3600";
                $tokenId = sha1($tm2);
                $_SESSION['tokenId'] = $tokenId;
                $_SESSION['tokenTime'] = $newtm;


                $sql = "insert into sessoes ";
                $sql .= "(id, token, token_time, cod_usuario) values(NULL, :token, :time, :user) ";
                $insere = $pdo->prepare($sql);
                $insere->bindValue(":token", $tokenId);
                $insere->bindValue(":time", $newtm);
                $insere->bindValue(":user", $dados['id']);
                $insere->execute();
                $sql = null;
                $consulta = null;

			}else{
                $_SESSION['fsh']=[
			        "flash"=>"Aguarde nossa Aprovação",
                    "type"=>"info",
                    ];
			}
		}
		if(isset($log)){
            $_SESSION['fsh']=[
			    "flash"=>"Logado com sucesso",
                "type"=>"success",
            ];

		}else{
			if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops, erro ao entrar, Digite e-mail e senha corretamente!!",
                    "type"=>"danger",
                ];
                header("Location: ?pg=Vlogin");
                exit();
			}
		}

		
	}

}

?>