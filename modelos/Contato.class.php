<?php 
class Contato{
    public function fnccontatonew($nome, $nascimento, $telefone, $endereco, $numero, $bairro, $referencia, $cidade, $email){
            //inserção no banco
            try{
                $sql="INSERT INTO contato (`id`, `nome`, `nascimento`, `telefone`, `endereco`, `numero`, `bairro`, `referencia`, `cidade`, `email`)"
                                 ." VALUES(NULL, :nome, :nascimento, :telefone, :endereco, :numero, :bairro, :referencia, :cidade, :email)";
                global $pdo;
                $insere=$pdo->prepare($sql);
                $insere->bindValue(":nome",$nome);
                $insere->bindValue(":nascimento",$nascimento);
                $insere->bindValue(":telefone",$telefone);
                $insere->bindValue(":endereco",$endereco);
                $insere->bindValue(":numero",$numero);
                $insere->bindValue(":bairro",$bairro);
                $insere->bindValue(":referencia",$referencia);
                $insere->bindValue(":cidade",$cidade);
                $insere->bindValue(":email",$email);
                $insere->execute();
            }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
            }

        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vcontato_lista");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao





    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccontatoedit($id, $nome, $nascimento, $telefone, $endereco, $numero, $bairro, $referencia, $cidade, $email){
        try{
            $sql="SELECT id FROM ";
            $sql.="contato";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute();
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){

            //inserção no banco
            try{
                $sql="UPDATE contato SET "
                    ."nome=:nome, "
                    ."nascimento=:nascimento,telefone=:telefone, "
                    ."endereco=:endereco,numero=:numero,bairro=:bairro,referencia=:referencia,cidade=:cidade, "
                    ."email=:email "
                    ."WHERE id=:id";
                global $pdo;
                $update=$pdo->prepare($sql);
                $update->bindValue(":nome",$nome);
                $update->bindValue(":nascimento",$nascimento);
                $update->bindValue(":telefone",$telefone);
                $update->bindValue(":endereco",$endereco);
                $update->bindValue(":numero",$numero);
                $update->bindValue(":bairro",$bairro);
                $update->bindValue(":referencia",$referencia);
                $update->bindValue(":cidade",$cidade);
                $update->bindValue(":email",$email);
                $update->bindValue(":id", $id);
                $update->execute();
            }catch ( PDOException $error_msg){
                echo 'Erro'. $error_msg->getMessage();
            }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse contato cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }

        if(isset($update)){
            /////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro Realizado Com Sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vcontato_lista");
            exit();

        }else{
            if(empty($_SESSION['fsh'])){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }

    }//fim da funcao



}//fim class
?>