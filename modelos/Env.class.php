<?php
//classe primaria
class Env{
    //determina se esta local ou em producao web
    //$_ENV['ENV'];

    //diretorio raiz (em alguns servers "web" "htdocs" "html")
    public $env_htmlpasta;

    //determina camminho no ponto abaixo da raiz
    public $env_root;

    //determina url na raiz
    public $env_url;

    //determina caminho url onde fica as images css e js imbarcados
    public $env_estatico;

    //nome da aplicacao
    public $env_nome;

    protected function __construct() {

        //atibuto t para testes
        //ou atributo p para producao
        $this->env_htmlpasta="html/";

        if ($_ENV['ENV']=="l"){
            $this->env_root=$_SERVER['DOCUMENT_ROOT']."/".$_ENV['ENV_SYS'];
            $this->env_url=$_ENV['ENV_URL'].$this->env_htmlpasta;
            $this->env_estatico=$this->env_url."public/";
        }

        //rodar sem dns
        if ($_ENV['ENV']=="p"){
            $this->env_root="/var/www/";
            $this->env_url="http://sisteminha.net/";
            $this->env_estatico="https://sisteminha.net/public/";
            if ($_SERVER['HTTP_HOST']=="159.89.42.146"){
                $this->env_url="http://159.89.42.146/";
                $this->env_estatico="http://159.89.42.146/public/";
            }

        }

        $this->env_nome=" Caf-e ";
    }

}
